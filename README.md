# Logistics App

An app to simulate a real world solution for maintaining logistics company.
Bussiness statement can be found in [documentation directory]("docs/zadanie.docx")

## Requirements
- JDK 17
- Maven 3.3+
- MySQL database (8+)

## How To Run
In order to download all dependencies it is recommended run maven as a first step using intellij or a shell command
>mvn clean install

## To do
Make a copy of `application-dev.yml_template` and name it to `application-dev.yml` and update `SETME`
values to your desires. 

This new file is covered by git so don't worry about accidentally pushing your credentials to repository.

After that run springboot with dev profile.

## Api docs

Api is documented usinng openApi and should be avaiable under `<baseUrl>/api/docs`

## How to change/set active/profiles
1. Edit Configuration
2. Spring Boot
3. App name 
4. Active profiles -> type the name
5. The properties file should have name -> application-profileName.yml - if you use yml file. 
6. Profile names - test/ dev/ prod.

## Links to government vat list API:
1. Main link to API information - https://www.gov.pl/web/kas/api-wykazu-podatnikow-vat
2. Test- https://wl-test.mf.gov.pl/ - this api may not have all the data to test the app.
3. Prod - https://wl-api.mf.gov.pl/
