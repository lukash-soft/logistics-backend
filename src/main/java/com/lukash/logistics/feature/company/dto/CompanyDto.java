package com.lukash.logistics.feature.company.dto;

import com.lukash.logistics.feature.company.model.Company;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CompanyDto {

    private Long id;
    private String companyName;
    private String address;
    private String taxNumber;
    private String accountNumber;

    public static CompanyDto of(Company company) {
        return new CompanyDto(
                company.getId(),
                company.getCompanyName(),
                company.getAddress(),
                company.getTaxNumber(),
                company.getAccountNumber()
        );
    }
}
