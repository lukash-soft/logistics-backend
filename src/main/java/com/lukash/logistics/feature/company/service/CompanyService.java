package com.lukash.logistics.feature.company.service;

import com.lukash.logistics.feature.company.dto.CompanyDto;
import com.lukash.logistics.feature.company.exception.CompanyAlreadyExistsException;
import com.lukash.logistics.feature.company.model.Company;
import com.lukash.logistics.feature.company.repository.CompanyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CompanyService {

    private final CompanyRepository companyRepository;

    public Optional<CompanyDto> get() {
        return companyRepository.findFirstBy().map(CompanyDto::of);
    }

    public Company save(CompanyDto companyDto) {
        if (companyRepository.count() >= 1) {
            throw new CompanyAlreadyExistsException();
        }

        Company companyToSave = Company.builder()
                .companyName(companyDto.getCompanyName())
                .address(companyDto.getAddress())
                .accountNumber(companyDto.getAccountNumber())
                .taxNumber(companyDto.getTaxNumber())
                .build();

        return companyRepository.save(companyToSave);
    }

    public void update(CompanyDto companyDto) {
        Company companyToUpdate = companyRepository.findById(companyDto.getId()).orElseThrow();

        companyToUpdate.setCompanyName(companyDto.getCompanyName());
        companyToUpdate.setAddress(companyDto.getAddress());
        companyToUpdate.setTaxNumber(companyDto.getTaxNumber());
        companyToUpdate.setAccountNumber(companyDto.getAccountNumber());

        companyRepository.save(companyToUpdate);
    }
}
