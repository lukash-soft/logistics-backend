package com.lukash.logistics.feature.company.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class CompanyAlreadyExistsException extends ApplicationException {
    public CompanyAlreadyExistsException() {
        super("Company already exists");
    }
}
