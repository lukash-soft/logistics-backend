package com.lukash.logistics.feature.company.repository;

import com.lukash.logistics.feature.company.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, Long> {
    Optional<Company> findFirstBy();

}
