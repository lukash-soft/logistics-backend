package com.lukash.logistics.feature.company.controller;

import com.lukash.logistics.feature.company.dto.CompanyDto;
import com.lukash.logistics.feature.company.service.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.lukash.logistics.configuration.security.constants.UserRole.RoleValues.ROLE_OWNER;

@RestController
@RequestMapping("companies")
@RequiredArgsConstructor
public class CompanyController {

    private final CompanyService companyService;

    @GetMapping
    public ResponseEntity<CompanyDto> get() {
        return ResponseEntity.of(companyService.get());
    }

    @Secured({ROLE_OWNER})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyDto save(@RequestBody CompanyDto companyDto) {
        return CompanyDto.of(companyService.save(companyDto));
    }

    @Secured({ROLE_OWNER})
    @PutMapping
    public void update(@RequestBody CompanyDto companyDto) {
        companyService.update(companyDto);
    }
}
