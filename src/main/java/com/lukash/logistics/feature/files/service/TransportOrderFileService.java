package com.lukash.logistics.feature.files.service;

import com.lukash.logistics.feature.files.dto.AddDamageFileDto;
import com.lukash.logistics.feature.files.dto.AddFileDto;
import com.lukash.logistics.feature.files.exception.DirectoryCreationException;
import com.lukash.logistics.feature.files.exception.FileException;
import com.lukash.logistics.feature.files.exception.OrderFileNotFoundException;
import com.lukash.logistics.feature.files.model.TransportOrderFile;
import com.lukash.logistics.feature.files.repository.TransportOrderFileRepository;
import com.lukash.logistics.feature.transport.exception.OrderAuthorizationException;
import com.lukash.logistics.feature.transport.exception.OrderNotFoundException;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.employee.exception.EmployeeNotFoundException;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;

@Service
@Slf4j
public class TransportOrderFileService {

    private final TransportOrderFileRepository transportOrderFileRepository;
    private final TransportOrderRepository transportOrderRepository;
    private final EmployeeRepository employeeRepository;

    private final String fileDirectory;

    public TransportOrderFileService(
            @Value("${files.orders}") String directory,
            EmployeeRepository employeeRepository,
            TransportOrderFileRepository transportOrderFileRepository,
            TransportOrderRepository transportOrderRepository) {
        this.fileDirectory = directory;
        this.employeeRepository = employeeRepository;
        this.transportOrderFileRepository = transportOrderFileRepository;
        this.transportOrderRepository = transportOrderRepository;
    }

    public List<TransportOrderFile> getSummary(Long orderId) {
        return transportOrderFileRepository.findAllByTransportOrderId(orderId);
    }

    public byte[] getById(Long orderFileId) {
        TransportOrderFile orderFile = transportOrderFileRepository.findById(orderFileId).orElseThrow(OrderFileNotFoundException::new);
        File file = new File(orderFile.getPath());

        return getBytesForFile(file);
    }

    private byte[] getBytesForFile(File file) {
        try (InputStream stream = new FileInputStream(file)) {
            return StreamUtils.copyToByteArray(stream);
        } catch (IOException e) {
            throw new FileException(e);
        }
    }

    public TransportOrderFile addDamageFile(AddDamageFileDto addDamageFileDto, MultipartFile file, final UserDetails userDetails) {
        final Employee currentUser = employeeRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(EmployeeNotFoundException::new);
        final TransportOrder transportOrder = transportOrderRepository
                .findByIdAndSupervisor(addDamageFileDto.getOrderId(), currentUser)
                .orElseThrow(OrderNotFoundException::new);

        final String storagePath = storeFileOnDisk(file, transportOrder);

        final TransportOrderFile orderFile = TransportOrderFile.builder()
                .name(file.getName())
                .addedBy(currentUser)
                .path(storagePath)
                .transportOrder(transportOrder)
                .description(addDamageFileDto.getDescription())
                .build();

        return transportOrderFileRepository.save(orderFile);
    }

    public TransportOrderFile addFile(AddFileDto addFileDto,  final UserDetails userDetails) {
        final Employee currentUser = employeeRepository.findByEmail(userDetails.getUsername()).orElseThrow(EmployeeNotFoundException::new);
        final TransportOrder transportOrder = transportOrderRepository.findByIdAndSupervisor(addFileDto.getOrderId(), currentUser).orElseThrow(OrderNotFoundException::new);
        final String storagePath = storeFileOnDisk(addFileDto.getFile(), transportOrder);

        final TransportOrderFile orderFile = TransportOrderFile.builder()
                .name(addFileDto.getFile().getName())
                .addedBy(currentUser)
                .path(storagePath)
                .description(addFileDto.getDescription())
                .transportOrder(transportOrder)
                .build();

        return transportOrderFileRepository.save(orderFile);
    }

    public void delete(Long orderFileId, UserDetails userDetails) {
        TransportOrderFile orderFileToDelete = transportOrderFileRepository.findById(orderFileId).orElseThrow(OrderNotFoundException::new);
        Employee supervisor = orderFileToDelete.getTransportOrder().getSupervisor();
        final Employee currentUser = employeeRepository.findByEmail(userDetails.getUsername()).orElseThrow(EmployeeNotFoundException::new);

        if (!currentUser.equals(supervisor)) {
            throw new OrderAuthorizationException();
        }

        deletePhysicalFile(orderFileToDelete);

        transportOrderFileRepository.deleteById(orderFileId);
    }

    private static void deletePhysicalFile(final TransportOrderFile orderFileToDelete) {
        try {
            Files.delete(Paths.get(orderFileToDelete.getPath()));
        } catch (IOException e) {
            log.error("Missing file associated with DB entry: {}", orderFileToDelete.getPath());
        }
    }

    @Scheduled(cron = "@monthly")
    @Transactional
    public void cleanUp() {
        LocalDate dueDate = LocalDate.now().minusMonths(12);
        List<TransportOrderFile> filesWithAgeOfTwelveMonths = transportOrderFileRepository.findAllByDateAddedIsBefore(dueDate);

        filesWithAgeOfTwelveMonths.forEach(TransportOrderFileService::deletePhysicalFile);

        transportOrderFileRepository.deleteAll(filesWithAgeOfTwelveMonths);
    }

    private String storeFileOnDisk(final MultipartFile multipartFile, final TransportOrder transportOrder) {
        try {
            final String destination = determineDestination(transportOrder, multipartFile);
            log.info("Storing file to a new location: {}", destination);
            multipartFile.transferTo(new File(destination));
            return destination;
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String determineDestination(final TransportOrder transportOrder, final MultipartFile multipartFile) {
        Path directoryPath = Paths.get(fileDirectory, transportOrder.getId().toString());

        File finalDirectory = new File(directoryPath.toString());
        createDirectoryIfNotExists(finalDirectory);

        return String.join(File.separator,
                directoryPath.toAbsolutePath().toString(),
                new Random().nextInt() + "-" + multipartFile.getOriginalFilename());
    }

    private void createDirectoryIfNotExists(File directory) {
        if (!directory.exists()) {
            try {
                Files.createDirectories(Paths.get(directory.getAbsolutePath()));
            } catch (IOException e) {
                throw new DirectoryCreationException(e);
            }
        }
    }
}
