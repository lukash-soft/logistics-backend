package com.lukash.logistics.feature.files.controller;

import com.lukash.logistics.feature.files.dto.AddFileDto;
import com.lukash.logistics.feature.files.dto.TransportOrderFileDto;
import com.lukash.logistics.feature.files.service.TransportOrderFileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("order/files")
public class TransportOrderFileController {

    private final TransportOrderFileService transportOrderFileService;

    @GetMapping("orderId/{orderId}/summary")
    public List<TransportOrderFileDto> getSummary(@PathVariable Long orderId) {
        return TransportOrderFileDto.ofList(transportOrderFileService.getSummary(orderId));
    }

    @GetMapping(produces = MediaType.ALL_VALUE, value = "id/{id}")
    public ResponseEntity<byte[]> getFileById(@PathVariable Long id) {
        byte[] fileInBytes = transportOrderFileService.getById(id);
        return ResponseEntity.ok()
                .contentType(MediaType.ALL)
                .body(fileInBytes);
    }

    @PostMapping
    public TransportOrderFileDto addFile(@ModelAttribute AddFileDto addFileDto,
                                         @AuthenticationPrincipal UserDetails userDetails) {
        return TransportOrderFileDto.of(transportOrderFileService.addFile(addFileDto, userDetails));
    }

    @DeleteMapping("id/{id}")
    public void delete(@PathVariable Long id, @AuthenticationPrincipal UserDetails userDetails) {
        transportOrderFileService.delete(id, userDetails);
    }
}
