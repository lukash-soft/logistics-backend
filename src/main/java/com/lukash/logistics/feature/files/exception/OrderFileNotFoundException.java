package com.lukash.logistics.feature.files.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class OrderFileNotFoundException extends ApplicationException {
    public OrderFileNotFoundException() {
        super("Order file not found");
    }
}
