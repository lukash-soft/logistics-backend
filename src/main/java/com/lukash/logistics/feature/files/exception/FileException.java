package com.lukash.logistics.feature.files.exception;

public class FileException extends RuntimeException {
    public FileException(Exception e) {
        super(e);
    }
}
