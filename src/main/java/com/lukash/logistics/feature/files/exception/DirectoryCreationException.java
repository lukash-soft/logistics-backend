package com.lukash.logistics.feature.files.exception;

public class DirectoryCreationException extends RuntimeException {
    public DirectoryCreationException(Throwable cause) {
        super(cause);
    }
}
