package com.lukash.logistics.feature.files.repository;

import com.lukash.logistics.feature.files.model.TransportOrderFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface TransportOrderFileRepository extends JpaRepository<TransportOrderFile, Long> {

    List<TransportOrderFile> findAllByTransportOrderId(Long orderId);

    List<TransportOrderFile> findAllByDateAddedIsBefore(LocalDate localDate);
}
