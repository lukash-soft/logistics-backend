package com.lukash.logistics.feature.files.dto;

import com.lukash.logistics.feature.files.model.TransportOrderFile;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransportOrderFileDto {
    private Long id;
    private String name;
    private LocalDate addedDate;
    private String description;

    public static TransportOrderFileDto of(TransportOrderFile orderFile) {
        return TransportOrderFileDto.builder()
                .id(orderFile.getId())
                .name(orderFile.getName())
                .description(orderFile.getDescription())
                .addedDate(orderFile.getDateAdded())
                .build();
    }

    public static List<TransportOrderFileDto> ofList(List<TransportOrderFile> files) {
        return files.stream()
                .map(TransportOrderFileDto::of)
                .collect(Collectors.toList());
    }
}
