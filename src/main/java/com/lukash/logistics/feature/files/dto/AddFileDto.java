package com.lukash.logistics.feature.files.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AddFileDto {
    private Long orderId;
    private String description;
    private MultipartFile file;
}
