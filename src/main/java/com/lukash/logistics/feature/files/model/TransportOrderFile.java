package com.lukash.logistics.feature.files.model;

import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.user.employee.model.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransportOrderFile {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String path;

    @Column(nullable = false)
    @Builder.Default
    private LocalDate dateAdded = LocalDate.now();

    @Column(nullable = false)
    private String description;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Employee addedBy;

    @ManyToOne(optional = false)
    private TransportOrder transportOrder;
}
