package com.lukash.logistics.feature.user.carrier.dto;

import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarrierDto {

    private Long id;
    private String companyName;
    private String address;
    private String taxNumber;
    private String accountNumber;
    private UserStatus userStatus = UserStatus.ACTIVE;

    public static CarrierDto of(Carrier carrier) {
        return new CarrierDto(
                carrier.getId(),
                carrier.getCompanyName(),
                carrier.getAddress(),
                carrier.getTaxNumber(),
                carrier.getAccountNumber(),
                carrier.getUserStatus()
        );
    }

    public static List<CarrierDto> ofList(List<Carrier> list) {
        return list.stream()
                .map(carrier -> CarrierDto.of(carrier))
                .collect(Collectors.toList());
    }
}
