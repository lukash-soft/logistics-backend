package com.lukash.logistics.feature.user.employee.model;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.user.employee.dto.EmployeeRegistrationDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @Builder.Default
    private UserRole role = UserRole.EMPLOYEE;

    @Column(nullable = false)
    private double salary;

    @Column(nullable = false)
    @Builder.Default
    private LocalDate dateStarted = LocalDate.now();

    private LocalDate dateEnded;

    @Column(nullable = false)
    private String password;

    public static Employee ofDto(EmployeeRegistrationDto registrationForm) {
        return Employee.builder()
                .firstName(registrationForm.getFirstName())
                .lastName(registrationForm.getLastName())
                .email(registrationForm.getEmail())
                .password(registrationForm.getPassword())
                .salary(registrationForm.getSalary())
                .dateStarted(registrationForm.getDateStarted())
                .dateEnded(registrationForm.getDateEnded())
                .role(registrationForm.getRole())
                .build();
    }

    public boolean isAccountActive() {
        final LocalDate today = LocalDate.now();

        if (dateStarted.isAfter(today)) {
            return false;
        }

        return dateEnded == null || dateEnded.isAfter(today);
    }
}
