package com.lukash.logistics.feature.user.client.repository;

import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Long> {
    List<Client> findAllByUserStatus(UserStatus userStatus);

    List<Client> findByIdIn(Collection<Long> ids);

}
