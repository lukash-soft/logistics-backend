package com.lukash.logistics.feature.user.carrier.repository;

import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface CarrierRepository extends JpaRepository<Carrier, Long> {

    List<Carrier> findAllByUserStatus(UserStatus userStatus);
    List<Carrier> findByIdIn(Collection<Long> ids);
}
