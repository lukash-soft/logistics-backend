package com.lukash.logistics.feature.user.client.controller;

import com.lukash.logistics.feature.user.client.dto.ClientDto;
import com.lukash.logistics.feature.user.client.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("clients")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

    @GetMapping
    public List<ClientDto> getAllActiveClients() {
        return ClientDto.ofList(clientService.getAllActiveClients());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClientDto save(@RequestBody ClientDto clientDto) {
        return ClientDto.of(clientService.save(clientDto));
    }

    @PutMapping
    public void update(@RequestBody ClientDto clientDto) {
        clientService.update(clientDto);
    }

    @DeleteMapping("clientId/{clientId}")
    public void delete(@PathVariable Long clientId) {
        clientService.delete(clientId);
    }
}
