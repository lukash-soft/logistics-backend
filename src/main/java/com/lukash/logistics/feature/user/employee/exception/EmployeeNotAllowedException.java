package com.lukash.logistics.feature.user.employee.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class EmployeeNotAllowedException extends ApplicationException {
    public EmployeeNotAllowedException() {
        super("You are not allowed to do this");
    }
}
