package com.lukash.logistics.feature.user.client.service;

import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.client.dto.ClientDto;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;
    private final TransportOrderRepository transportOrderRepository;

    public List<Client> getAllActiveClients() {
        return clientRepository.findAllByUserStatus(UserStatus.ACTIVE);
    }

    public Client save(ClientDto clientDto) {
        Client clientToSave = Client.builder()
                .companyName(clientDto.getCompanyName())
                .address(clientDto.getAddress())
                .accountNumber(clientDto.getAccountNumber())
                .taxNumber(clientDto.getTaxNumber())
                .build();

        return clientRepository.save(clientToSave);
    }

    public void update(ClientDto clientDto) {
        Client clientToUpdate = clientRepository.findById(clientDto.getId()).orElseThrow();

        clientToUpdate.setCompanyName(clientDto.getCompanyName());
        clientToUpdate.setAddress(clientDto.getAddress());
        clientToUpdate.setAccountNumber(clientDto.getAccountNumber());
        clientToUpdate.setTaxNumber(clientDto.getTaxNumber());

        clientRepository.save(clientToUpdate);
    }

    public void delete(Long clientId) {
        if (transportOrderRepository.countTransportOrderByClientId(clientId) == 0) {
            clientRepository.deleteById(clientId);
        } else {
            clientRepository.findById(clientId).ifPresent(ClientService::anonymizeClient);
        }
    }

    private static void anonymizeClient(Client client) {
        client.setCompanyName(client.getCompanyName().substring(0, 1));
        client.setAddress(client.getAddress().substring(0, 1));
        client.setAccountNumber(client.getAccountNumber().substring(0, 1));
        client.setTaxNumber(client.getTaxNumber().substring(0, 1));
        client.setUserStatus(UserStatus.DISABLED);
    }
}
