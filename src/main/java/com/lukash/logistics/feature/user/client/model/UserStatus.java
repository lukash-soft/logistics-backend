package com.lukash.logistics.feature.user.client.model;

public enum UserStatus {
    ACTIVE, DISABLED
}
