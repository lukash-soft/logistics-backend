package com.lukash.logistics.feature.user.employee.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeUpdateNameDto {
    private Long id;
    private String firstName;
    private String lastName;

}
