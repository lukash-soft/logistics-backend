package com.lukash.logistics.feature.user.employee.dto;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.user.employee.model.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private UserRole role;
    private double salary;
    private LocalDate dateStarted;
    private LocalDate dateEnded;

    public static EmployeeDto of(Employee employee) {
        return new EmployeeDto(
                employee.getId(),
                employee.getFirstName(),
                employee.getLastName(),
                employee.getEmail(),
                employee.getRole(),
                employee.getSalary(),
                employee.getDateStarted(),
                employee.getDateEnded()
        );
    }

    public static List<EmployeeDto> ofList(List<Employee> employees) {
        return employees.stream()
                .map(employee -> EmployeeDto.of(employee))
                .collect(Collectors.toList());
    }
}
