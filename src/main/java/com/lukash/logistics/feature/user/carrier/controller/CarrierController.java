package com.lukash.logistics.feature.user.carrier.controller;

import com.lukash.logistics.feature.user.carrier.dto.CarrierDto;
import com.lukash.logistics.feature.user.carrier.service.CarrierService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("carriers")
@RequiredArgsConstructor
public class CarrierController {

    private final CarrierService carrierService;

    @GetMapping
    public List<CarrierDto> getAllActive() {
        return CarrierDto.ofList(carrierService.getAllActive());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CarrierDto save(@RequestBody CarrierDto carrierDto) {
        return CarrierDto.of(carrierService.save(carrierDto));
    }

    @PutMapping
    public void update(@RequestBody CarrierDto carrierDto) {
        carrierService.update(carrierDto);
    }

    @DeleteMapping("carrierId/{carrierId}")
    public void delete(@PathVariable Long carrierId) {
        carrierService.delete(carrierId);
    }
}
