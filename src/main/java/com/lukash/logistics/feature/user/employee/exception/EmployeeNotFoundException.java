package com.lukash.logistics.feature.user.employee.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class EmployeeNotFoundException extends ApplicationException {

    public EmployeeNotFoundException() {
        super("User not found");
    }

    public EmployeeNotFoundException(String message) {
        super(message);
    }
}
