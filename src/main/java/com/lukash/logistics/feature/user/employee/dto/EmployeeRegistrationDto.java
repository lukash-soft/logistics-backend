package com.lukash.logistics.feature.user.employee.dto;

import com.lukash.logistics.configuration.security.constants.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeRegistrationDto {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private UserRole role;
    private double salary;
    private LocalDate dateStarted;
    private LocalDate dateEnded;
}
