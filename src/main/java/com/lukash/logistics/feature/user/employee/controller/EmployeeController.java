package com.lukash.logistics.feature.user.employee.controller;

import com.lukash.logistics.feature.user.employee.dto.EmployeeDto;
import com.lukash.logistics.feature.user.employee.dto.EmployeeRegistrationDto;
import com.lukash.logistics.feature.user.employee.dto.EmployeeUpdateNameDto;
import com.lukash.logistics.feature.user.employee.dto.EmployeeUpdateSalaryDto;
import com.lukash.logistics.feature.user.employee.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.lukash.logistics.configuration.security.constants.UserRole.RoleValues.ROLE_MANAGER;
import static com.lukash.logistics.configuration.security.constants.UserRole.RoleValues.ROLE_OWNER;

@RestController
@RequestMapping("employees")
@RequiredArgsConstructor
@Secured({ROLE_MANAGER, ROLE_OWNER})
public class EmployeeController {
    private final EmployeeService employeeService;

    @GetMapping
    public List<EmployeeDto> getUsers(@RequestParam(required = false, defaultValue = "true") boolean activeOnly) {
        return EmployeeDto.ofList(employeeService.getUsers(activeOnly));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeeDto save(@RequestBody EmployeeRegistrationDto employeeRegistrationDto) {
        return EmployeeDto.of(employeeService.save(employeeRegistrationDto));
    }

    @PutMapping("name")
    public void updateNames(@RequestBody EmployeeUpdateNameDto employeeDto) {
        employeeService.updateName(employeeDto);
    }

    @PutMapping("salary")
    public void updateSalary(@RequestBody EmployeeUpdateSalaryDto employeeDto) {
        employeeService.updateSalary(employeeDto);
    }

    @DeleteMapping("id/{id}")
    public void terminateEmployee(@PathVariable Long id) {
        employeeService.terminate(id);
    }

}
