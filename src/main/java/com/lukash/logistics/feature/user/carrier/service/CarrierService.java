package com.lukash.logistics.feature.user.carrier.service;

import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.carrier.dto.CarrierDto;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarrierService {

    private final CarrierRepository carrierRepository;
    private final TransportOrderRepository transportOrderRepository;

    public List<Carrier> getAllActive() {
        return carrierRepository.findAllByUserStatus(UserStatus.ACTIVE);
    }

    public Carrier save(CarrierDto carrierDto) {
        Carrier carrierToSave = Carrier.builder()
                .companyName(carrierDto.getCompanyName())
                .address(carrierDto.getAddress())
                .taxNumber(carrierDto.getTaxNumber())
                .accountNumber(carrierDto.getAccountNumber())
                .build();

        return carrierRepository.save(carrierToSave);
    }

    public void update(CarrierDto carrierDto) {
        Carrier carrierToUpdate = carrierRepository.findById(carrierDto.getId()).orElseThrow();

        carrierToUpdate.setCompanyName(carrierDto.getCompanyName());
        carrierToUpdate.setAddress(carrierDto.getAddress());
        carrierToUpdate.setAccountNumber(carrierDto.getAccountNumber());
        carrierToUpdate.setTaxNumber(carrierDto.getTaxNumber());

        carrierRepository.save(carrierToUpdate);
    }

    public void delete(Long carrierId) {
        if (transportOrderRepository.countTransportOrderByCarrierId(carrierId) == 0) {
            carrierRepository.deleteById(carrierId);
        } else {
            carrierRepository.findById(carrierId).ifPresent(CarrierService::anonymizeCarrier);
        }
    }

    private static void anonymizeCarrier(Carrier carrier) {
        carrier.setCompanyName(carrier.getCompanyName().substring(0, 1));
        carrier.setAddress(carrier.getAddress().substring(0, 1));
        carrier.setAccountNumber(carrier.getAccountNumber().substring(0, 1));
        carrier.setTaxNumber(carrier.getTaxNumber().substring(0, 1));
    }
}
