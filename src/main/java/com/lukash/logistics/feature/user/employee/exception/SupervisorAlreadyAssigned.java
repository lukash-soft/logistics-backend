package com.lukash.logistics.feature.user.employee.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class SupervisorAlreadyAssigned extends ApplicationException {
    public SupervisorAlreadyAssigned() {
        super("You cannot assign yourself twice");
    }
}
