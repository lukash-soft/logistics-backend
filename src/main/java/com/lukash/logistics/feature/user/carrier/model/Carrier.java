package com.lukash.logistics.feature.user.carrier.model;

import com.lukash.logistics.feature.user.carrier.dto.CarrierDto;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.vat.model.BusinessEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Carrier implements BusinessEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String companyName;

    @Column(nullable = false)
    private String address;

    @Column(unique = true, nullable = false)
    private String taxNumber;

    @Column(unique = true, nullable = false)
    private String accountNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @Builder.Default
    private UserStatus userStatus = UserStatus.ACTIVE;

    public static Carrier ofDto(CarrierDto carrierDto) {
        return Carrier.builder()
                .companyName(carrierDto.getCompanyName())
                .address(carrierDto.getAddress())
                .taxNumber(carrierDto.getTaxNumber())
                .accountNumber(carrierDto.getTaxNumber())
                .userStatus(carrierDto.getUserStatus())
                .build();
    }

}
