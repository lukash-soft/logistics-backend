package com.lukash.logistics.feature.user.employee.service;

import com.lukash.logistics.configuration.security.service.UserService;
import com.lukash.logistics.feature.user.employee.dto.PasswordChangeDto;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserPasswordService {
    private static final String DEFAULT_PASSWORD = "welcome";

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final EmployeeRepository employeeRepository;


    public void resetPassword(String email) {
        employeeRepository.findByEmail(email).ifPresent(emp -> {
                    emp.setPassword(DEFAULT_PASSWORD);
                    employeeRepository.save(emp);
                }
        );
    }

    public void updatePassword(PasswordChangeDto passwordChangeDto) {
        Employee employee = userService.getLoggedAccount();

        final String encoded = passwordEncoder.encode(passwordChangeDto.getNewPassword());
        employee.setPassword(encoded);

        employeeRepository.save(employee);
    }

}
