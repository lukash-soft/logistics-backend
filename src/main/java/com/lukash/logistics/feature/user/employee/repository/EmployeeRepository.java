package com.lukash.logistics.feature.user.employee.repository;

import com.lukash.logistics.feature.user.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> findAllByDateStartedEqualsOrDateStartedBeforeAndDateEndedIsNullOrDateEndedIsAfter(LocalDate firstDate, LocalDate secondDate, LocalDate dateThird);

    Optional<Employee> findByEmail(String email);
}
