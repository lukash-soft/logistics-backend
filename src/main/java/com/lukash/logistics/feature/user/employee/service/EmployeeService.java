package com.lukash.logistics.feature.user.employee.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.configuration.security.exception.AuthException;
import com.lukash.logistics.feature.user.employee.dto.EmployeeRegistrationDto;
import com.lukash.logistics.feature.user.employee.dto.EmployeeUpdateNameDto;
import com.lukash.logistics.feature.user.employee.dto.EmployeeUpdateSalaryDto;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final PasswordEncoder encoder;

    public List<Employee> getUsers(boolean activeOnly) {
        if (activeOnly) {
            LocalDate today = LocalDate.now();
            return employeeRepository.findAllByDateStartedEqualsOrDateStartedBeforeAndDateEndedIsNullOrDateEndedIsAfter(today, today, today);
        }
        return employeeRepository.findAll();

    }

    public Employee save(EmployeeRegistrationDto registrationForm) {
        Employee employeeToSave = Employee.ofDto(registrationForm);

        if (employeeToSave.getRole().equals(UserRole.OWNER)) {
            throw new AuthException("You can't add employee with this role.");
        }

        final String encoded = encoder.encode("password");
        employeeToSave.setPassword(encoded);

        return employeeRepository.save(employeeToSave);
    }

    public void updateName(EmployeeUpdateNameDto employeeUpdateNameDto) {
        Employee employee = employeeRepository.findById(employeeUpdateNameDto.getId()).orElseThrow();

        employee.setFirstName(employeeUpdateNameDto.getFirstName());
        employee.setLastName(employeeUpdateNameDto.getLastName());

        employeeRepository.save(employee);
    }

    public void updateSalary(EmployeeUpdateSalaryDto employeeUpdateSalaryDto) {
        Employee employee = employeeRepository.findById(employeeUpdateSalaryDto.getId()).orElseThrow();

        employee.setSalary(employeeUpdateSalaryDto.getSalary());

        employeeRepository.save(employee);
    }

    public void terminate(Long employeeToTerminateId) {
        Employee employee = employeeRepository.findById(employeeToTerminateId).orElseThrow();
        if (employee.getRole().equals(UserRole.OWNER)) {
            throw new AuthException("You cannot terminate owner.");
        }
        employee.setDateEnded(LocalDate.now());

        employeeRepository.save(employee);
    }
}
