package com.lukash.logistics.feature.user.client.dto;

import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientDto {

    private Long id;
    private String companyName;
    private String address;
    private String taxNumber;
    private String accountNumber;
    private UserStatus userStatus;

    public static ClientDto of(Client client) {
        return new ClientDto(
                client.getId(),
                client.getCompanyName(),
                client.getAddress(),
                client.getTaxNumber(),
                client.getAccountNumber(),
                client.getUserStatus()
        );
    }

    public static List<ClientDto> ofList(List<Client> clients) {
        return clients.stream().map(ClientDto::of).collect(Collectors.toList());
    }
}
