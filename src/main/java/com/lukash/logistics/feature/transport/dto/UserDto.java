package com.lukash.logistics.feature.transport.dto;

import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.employee.model.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    private Long id;
    private String name;

    //ToDo: #25 introduce User interface
    public static UserDto of(Client client) {
        return new UserDto(
                client.getId(),
                client.getCompanyName()
        );
    }

    public static UserDto of(Carrier carrier) {
        return new UserDto(
                carrier.getId(),
                carrier.getCompanyName()
        );
    }

    public static UserDto of(Employee employee) {
        return new UserDto(
                employee.getId(),
                employee.getFirstName() + " " + employee.getLastName()
        );
    }
}
