package com.lukash.logistics.feature.transport.dto;

import com.lukash.logistics.feature.transport.model.Cargo;
import com.lukash.logistics.feature.transport.model.enums.QuantityType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CargoDto {
    private String description;
    private String cargoType;
    private double quantity;
    private QuantityType quantityType;

    public static CargoDto of(Cargo cargo) {
        return CargoDto.builder()
                .cargoType(cargo.getCargoType())
                .description(cargo.getDescription())
                .quantity(cargo.getQuantity())
                .quantityType(cargo.getQuantityType())
                .build();
    }
}
