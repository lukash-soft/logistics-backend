package com.lukash.logistics.feature.transport.service;

import com.lukash.logistics.feature.transport.exception.OrderAuthorizationException;
import com.lukash.logistics.feature.transport.exception.OrderStatusNotAppropriate;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.dto.TransportRegistrationDto;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.LocalDate;

@Service
public class OrderValidationService {

    void validatePlannedDates(TransportRegistrationDto transportRegistrationDto) {
        if (transportRegistrationDto.getDateStartedPlanned().isAfter(transportRegistrationDto.getDateEndedPlanned())) {
            throw new DateTimeException("Starting date is after date planned.");
        }
    }

    void validateRealDates(TransportOrder transportOrder) {
        if (transportOrder.getDateEndedReal() != null && transportOrder.getDateStartedReal().isAfter(transportOrder.getDateEndedReal())) {
            throw new DateTimeException("Real start date is after real date.");
        }
    }

    void changeStatusValidator(TransportStatus newTransportStatus, TransportOrder transportOrderToUpdate) {
        TransportStatus formerTransportStatus = transportOrderToUpdate.getStatus();

        switch (newTransportStatus) {
            case CREATED, PAUSED -> {
                if (!formerTransportStatus.equals(TransportStatus.IN_PROGRESS)) {
                    throw new OrderStatusNotAppropriate("Incorrect status");
                }
            }
            case CANCELLED -> {
                if (formerTransportStatus.equals(TransportStatus.COMPLETE)) {
                    throw new OrderStatusNotAppropriate("Incorrect status");
                }
            }
        }
    }

    void deliveryDateValidator(LocalDate deliveryDate, TransportOrder transportOrder) {
        if (deliveryDate.isBefore(transportOrder.getDateStartedPlanned()) || deliveryDate.isAfter(LocalDate.now())) {
            throw new DateTimeException("Real delivery date is before start date.");
        }
    }

    void deleteStatusValidator(TransportOrder transportOrder) {
        if (!transportOrder.getStatus().equals(TransportStatus.CREATED)) {
            throw new OrderStatusNotAppropriate();
        }
    }

    public void validateSupervisor(UserDetails details, TransportOrder transportOrder) {
        if (!transportOrder.getSupervisor().getEmail().equals(details.getUsername())) {
            throw new OrderAuthorizationException();
        }
    }
}
