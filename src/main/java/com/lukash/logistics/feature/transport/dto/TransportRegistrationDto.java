package com.lukash.logistics.feature.transport.dto;

import com.lukash.logistics.feature.transport.model.enums.CurrencyModel;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransportRegistrationDto {
    private Long carrierId;
    private String addressFrom;
    private String addressTo;
    private LocalDate dateStartedPlanned;
    private LocalDate dateStartedReal;
    private LocalDate dateEndedPlanned;
    private LocalDate dateEndedReal;
    private CargoDto cargoDto;
    private double carrierFee;
    private double totalPrice;
    @Builder.Default
    private CurrencyModel currency = CurrencyModel.PLN;
    private TransportStatus status;
    private String vehicleNumber;
    private Long clientId;
}
