package com.lukash.logistics.feature.transport.scheduled;

import com.lukash.logistics.feature.report.service.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ScheduledTask {
    private final ReportService reportService;

    @Scheduled(cron = "0 1 1 * ?")
    void generateMonthlyReport() {
        LocalDate today = LocalDate.now();
        LocalDate reportDate = today.withDayOfMonth(1).minusMonths(1);
        reportService.generateReport(reportDate);
    }

}
