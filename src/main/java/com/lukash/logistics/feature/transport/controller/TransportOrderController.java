package com.lukash.logistics.feature.transport.controller;

import com.lukash.logistics.feature.transport.dto.OrderCompleteDto;
import com.lukash.logistics.feature.transport.dto.OrderSearchCriteria;
import com.lukash.logistics.feature.transport.dto.StatusUpdateDto;
import com.lukash.logistics.feature.transport.dto.TransportOrderDto;
import com.lukash.logistics.feature.transport.dto.TransportRegistrationDto;
import com.lukash.logistics.feature.transport.service.TransportOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("orders")
public class TransportOrderController {

    private final TransportOrderService transportOrderService;

    @GetMapping
    public List<TransportOrderDto> getAll() {
        return TransportOrderDto.ofList(transportOrderService.getAll());
    }

    @GetMapping("advanced")
    public List<TransportOrderDto> getAllBetweenDatesAndClient(@RequestParam Map<String, String> params) {
        OrderSearchCriteria orderSearchCriteria = OrderSearchCriteria.ofMap(params);

        return TransportOrderDto.ofList(transportOrderService.getAllByDatesAndClient(orderSearchCriteria));
    }

    @PostMapping
    public TransportOrderDto save(@RequestBody TransportRegistrationDto transportRegistrationDto, @AuthenticationPrincipal UserDetails details) {
        return TransportOrderDto.of(transportOrderService.save(transportRegistrationDto, details));
    }

    @PostMapping("batch")
    public List<TransportOrderDto> batchUpload(@RequestPart("file") MultipartFile file, @AuthenticationPrincipal UserDetails userDetails) {
        return TransportOrderDto.ofList(transportOrderService.saveBatch(file, userDetails));
    }

    @PutMapping("status")
    public void updateStatus(@RequestBody StatusUpdateDto statusUpdateDto, @AuthenticationPrincipal UserDetails userDetails) {
        transportOrderService.updateStatus(statusUpdateDto.getId(), statusUpdateDto.getTransportStatus(), userDetails);
    }

    @PutMapping("orderId/{orderId}/supervisor/")
    public void updateSupervisor(@PathVariable Long orderId, @AuthenticationPrincipal UserDetails details) {
        transportOrderService.updateSupervisor(orderId, details);
    }

    @PutMapping
    public void update(@RequestBody TransportOrderDto transportOrderDto, @AuthenticationPrincipal UserDetails userDetails) {
        transportOrderService.update(transportOrderDto, userDetails);
    }

    @DeleteMapping("orderId/{orderId}")
    public void delete(@PathVariable Long orderId, @AuthenticationPrincipal UserDetails details) {
        transportOrderService.deleteOrder(orderId, details);
    }

    @PutMapping("complete")
    public void completeOrder(@RequestBody OrderCompleteDto orderCompleteDto, @AuthenticationPrincipal UserDetails details) {
        transportOrderService.completeOrder(orderCompleteDto.getOrderId(), orderCompleteDto.getDeliveryDate(), details);
    }

}
