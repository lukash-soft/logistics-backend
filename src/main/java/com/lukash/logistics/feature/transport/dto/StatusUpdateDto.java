package com.lukash.logistics.feature.transport.dto;

import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class StatusUpdateDto {
    private Long id;
    private TransportStatus transportStatus;

    public static StatusUpdateDto of(TransportOrder transportOrder) {
        return StatusUpdateDto.builder()
                .id(transportOrder.getId())
                .transportStatus(transportOrder.getStatus())
                .build();
    }
}
