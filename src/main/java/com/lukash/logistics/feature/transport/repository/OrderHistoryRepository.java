package com.lukash.logistics.feature.transport.repository;

import com.lukash.logistics.feature.transport.model.OrderHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderHistoryRepository extends JpaRepository<OrderHistory, Long> {
}
