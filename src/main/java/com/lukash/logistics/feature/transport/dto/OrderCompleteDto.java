package com.lukash.logistics.feature.transport.dto;

import com.lukash.logistics.feature.transport.model.TransportOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class OrderCompleteDto {
    private Long orderId;
    @Builder.Default
    private LocalDate deliveryDate = LocalDate.now();

    public static OrderCompleteDto of(TransportOrder transportOrder) {
        return OrderCompleteDto.builder()
                .orderId(transportOrder.getId())
                .deliveryDate(transportOrder.getDateEndedReal())
                .build();
    }
}
