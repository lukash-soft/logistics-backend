package com.lukash.logistics.feature.transport.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class OrderStatusNotAppropriate extends ApplicationException {

    public OrderStatusNotAppropriate() {
        super("Wrong order status to delete");
    }

    public OrderStatusNotAppropriate(String message) {
        super(message);
    }
}
