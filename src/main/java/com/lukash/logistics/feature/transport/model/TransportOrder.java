package com.lukash.logistics.feature.transport.model;

import com.lukash.logistics.feature.transport.model.enums.CurrencyModel;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.employee.model.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TransportOrder {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false)
    private Carrier carrier;

    @Column(nullable = false)
    private String addressFrom;

    @Column(nullable = false)
    private String addressTo;

    @Column(nullable = false)
    private LocalDate dateStartedPlanned;

    @Column
    private LocalDate dateStartedReal;

    @Column(nullable = false)
    private LocalDate dateEndedPlanned;

    @Column
    private LocalDate dateEndedReal;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    private Cargo cargo;

    @Column(nullable = false)
    @Min(0)
    private double carrierFee;

    @Column(nullable = false)
    @Min(0)
    private double totalPrice;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @Builder.Default
    private CurrencyModel currency = CurrencyModel.PLN;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @Builder.Default
    private TransportStatus status = TransportStatus.CREATED;

    @ManyToOne(optional = false)
    private Employee supervisor;

    @Column(nullable = false)
    private String vehicleNumber;

    @ManyToOne(optional = false)
    private Client client;
}
