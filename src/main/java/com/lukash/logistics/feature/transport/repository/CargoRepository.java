package com.lukash.logistics.feature.transport.repository;

import com.lukash.logistics.feature.transport.model.Cargo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CargoRepository extends JpaRepository<Cargo, Long> {
}
