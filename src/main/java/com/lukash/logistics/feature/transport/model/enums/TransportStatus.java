package com.lukash.logistics.feature.transport.model.enums;

import java.util.Set;

public enum TransportStatus {
    CREATED, IN_PROGRESS, PAUSED, CANCELLED, COMPLETE;

    public static final Set<TransportStatus> ACTIVE_STATUSES = Set.of(CREATED, PAUSED, IN_PROGRESS);
}
