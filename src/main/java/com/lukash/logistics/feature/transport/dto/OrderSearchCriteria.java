package com.lukash.logistics.feature.transport.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderSearchCriteria {
    private static final LocalDate DEFAULT_PLANNED_DATE_FROM = LocalDate.of(1900, 1, 1);
    private static final LocalDate DEFAULT_PLANNED_DATE_TO = LocalDate.of(3000, 1, 1);

    @Builder.Default
    private LocalDate plannedStartFrom = DEFAULT_PLANNED_DATE_FROM;
    @Builder.Default
    private LocalDate plannedStartTo = DEFAULT_PLANNED_DATE_TO;
    @Builder.Default
    private Long client = null;

    public static OrderSearchCriteria ofMap(Map<String, String> params) {
        LocalDate dateStartFrom = Optional.ofNullable(params.get("plannedStartFrom"))
                .map(LocalDate::parse)
                .orElse(DEFAULT_PLANNED_DATE_FROM);

        LocalDate dateStartTo = Optional.ofNullable(params.get("plannedStartTo"))
                .map(LocalDate::parse)
                .orElse(DEFAULT_PLANNED_DATE_TO);

        Long clientId = Optional.ofNullable(params.get("clientId"))
                .map(Long::parseLong)
                .orElse(null);

        return OrderSearchCriteria.builder()
                .plannedStartFrom(dateStartFrom)
                .plannedStartTo(dateStartTo)
                .client(clientId)
                .build();
    }
}
