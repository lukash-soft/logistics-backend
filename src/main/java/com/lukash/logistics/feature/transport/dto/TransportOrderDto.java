package com.lukash.logistics.feature.transport.dto;

import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.model.enums.CurrencyModel;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class TransportOrderDto {
    private Long id;
    private UserDto carrier;
    private String addressFrom;
    private String addressTo;
    private LocalDate dateStartedPlanned;
    private LocalDate dateStartedReal;
    private LocalDate dateEndedPlanned;
    private LocalDate dateEndedReal;
    private CargoDto cargoDto;
    private double carrierFee;
    private double totalPrice;
    private CurrencyModel currency;
    private TransportStatus status;
    private UserDto supervisor;
    private String vehicleNumber;
    private UserDto client;

    public static TransportOrderDto of(TransportOrder transportOrder) {
        return new TransportOrderDto(
                transportOrder.getId(),
                UserDto.of(transportOrder.getCarrier()),
                transportOrder.getAddressFrom(),
                transportOrder.getAddressTo(),
                transportOrder.getDateStartedPlanned(),
                transportOrder.getDateStartedReal(),
                transportOrder.getDateEndedPlanned(),
                transportOrder.getDateEndedReal(),
                CargoDto.of(transportOrder.getCargo()),
                transportOrder.getCarrierFee(),
                transportOrder.getTotalPrice(),
                transportOrder.getCurrency(),
                transportOrder.getStatus(),
                UserDto.of(transportOrder.getSupervisor()),
                transportOrder.getVehicleNumber(),
                UserDto.of(transportOrder.getClient())
        );
    }


    public static List<TransportOrderDto> ofList(List<TransportOrder> transportOrders) {
        return transportOrders.stream().map(TransportOrderDto::of).collect(Collectors.toList());
    }
}
