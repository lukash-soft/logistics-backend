package com.lukash.logistics.feature.transport.repository;

import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import com.lukash.logistics.feature.user.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface TransportOrderRepository extends JpaRepository<TransportOrder, Long> {
    List<TransportOrder> findByStatusAndDateEndedRealIs(TransportStatus transportStatus, LocalDate date);

    List<TransportOrder> findByDateStartedPlannedIsAfterAndDateEndedPlannedIsBefore(LocalDate from, LocalDate to);

    List<TransportOrder> findByDateStartedPlannedIsAfterAndDateEndedPlannedIsBeforeAndClientId(LocalDate from, LocalDate to, Long clientId);

    List<TransportOrder> findByStatusIn(Collection<TransportStatus> statuses);

    List<TransportOrder> findByStatusAndDateEndedRealIsBetween(TransportStatus transportStatus, LocalDate startDate, LocalDate endDate);

    int countTransportOrderByCarrierId(Long carrierId);

    int countTransportOrderByClientId(Long clientId);

    Optional<TransportOrder> findByIdAndSupervisor(Long orderId, Employee employee);
}
