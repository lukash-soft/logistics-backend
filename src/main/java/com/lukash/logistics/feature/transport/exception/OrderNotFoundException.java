package com.lukash.logistics.feature.transport.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class OrderNotFoundException extends ApplicationException {
    public OrderNotFoundException() {
        super("Order not found");
    }
}
