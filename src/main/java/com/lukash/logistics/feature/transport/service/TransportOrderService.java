package com.lukash.logistics.feature.transport.service;

import com.lukash.logistics.feature.invoice.InvoiceService;
import com.lukash.logistics.feature.transport.dto.OrderSearchCriteria;
import com.lukash.logistics.feature.transport.dto.TransportOrderCsvDTO;
import com.lukash.logistics.feature.transport.dto.TransportOrderDto;
import com.lukash.logistics.feature.transport.dto.TransportRegistrationDto;
import com.lukash.logistics.feature.transport.exception.CsvException;
import com.lukash.logistics.feature.transport.exception.OrderNotFoundException;
import com.lukash.logistics.feature.transport.model.Cargo;
import com.lukash.logistics.feature.transport.model.OrderHistory;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import com.lukash.logistics.feature.transport.repository.CargoRepository;
import com.lukash.logistics.feature.transport.repository.OrderHistoryRepository;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.feature.user.employee.exception.EmployeeNotFoundException;
import com.lukash.logistics.feature.user.employee.exception.SupervisorAlreadyAssigned;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransportOrderService {

    private final InvoiceService invoiceService;
    private final TransportOrderRepository transportOrderRepository;
    private final EmployeeRepository employeeRepository;
    private final OrderHistoryRepository orderHistoryRepository;
    private final OrderValidationService orderValidationService;
    private final CarrierRepository carrierRepository;
    private final CargoRepository cargoRepository;
    private final ClientRepository clientRepository;

    public List<TransportOrder> getAll() {
        return transportOrderRepository.findAll();
    }

    @Transactional
    public TransportOrder save(TransportRegistrationDto transportRegistrationDto, UserDetails userDetails) {
        orderValidationService.validatePlannedDates(transportRegistrationDto);

        Carrier carrierFromDto = carrierRepository.findById(transportRegistrationDto.getCarrierId()).orElseThrow();
        Client clientFromDto = clientRepository.findById(transportRegistrationDto.getClientId()).orElseThrow();
        Employee supervisor = employeeRepository.findByEmail(userDetails.getUsername()).orElseThrow(EmployeeNotFoundException::new);

        Cargo cargo = saveCargoFromRegistrationDto(transportRegistrationDto);

        TransportOrder transportOrderToSave = TransportOrder.builder()
                .cargo(cargo)
                .carrier(carrierFromDto)
                .client(clientFromDto)
                .addressFrom(transportRegistrationDto.getAddressFrom())
                .addressTo(transportRegistrationDto.getAddressTo())
                .supervisor(supervisor)
                .vehicleNumber(transportRegistrationDto.getVehicleNumber())
                .dateStartedPlanned(transportRegistrationDto.getDateStartedPlanned())
                .dateEndedPlanned(transportRegistrationDto.getDateEndedPlanned())
                .build();

        OrderHistory orderHistory = OrderHistory.builder()
                .transportOrder(transportOrderToSave)
                .transportStatus(TransportStatus.CREATED)
                .changedBy(supervisor)
                .build();
        transportOrderRepository.save(transportOrderToSave);
        orderHistoryRepository.save(orderHistory);

        return transportOrderToSave;
    }

    private Cargo saveCargoFromRegistrationDto(TransportRegistrationDto transportRegistrationDto) {
        Cargo cargo = Cargo.builder()
                .cargoType(transportRegistrationDto.getCargoDto().getCargoType())
                .description(transportRegistrationDto.getCargoDto().getDescription())
                .quantityType(transportRegistrationDto.getCargoDto().getQuantityType())
                .quantity(transportRegistrationDto.getCargoDto().getQuantity())
                .build();
        return cargoRepository.save(cargo);
    }

    public List<TransportOrder> getAllByDatesAndClient(OrderSearchCriteria orderSearchCriteria) {
        if (orderSearchCriteria.getClient() == null) {
            return transportOrderRepository.findByDateStartedPlannedIsAfterAndDateEndedPlannedIsBefore(
                    orderSearchCriteria.getPlannedStartFrom(),
                    orderSearchCriteria.getPlannedStartTo());
        }

        return transportOrderRepository.findByDateStartedPlannedIsAfterAndDateEndedPlannedIsBeforeAndClientId(
                orderSearchCriteria.getPlannedStartFrom(),
                orderSearchCriteria.getPlannedStartTo(),
                orderSearchCriteria.getClient());
    }

    @Transactional
    public void updateStatus(Long orderId, TransportStatus newTransportStatus, UserDetails details) {
        TransportOrder transportOrderToUpdate = transportOrderRepository.findById(orderId).orElseThrow(OrderNotFoundException::new);

        orderValidationService.validateSupervisor(details, transportOrderToUpdate);

        orderValidationService.changeStatusValidator(newTransportStatus, transportOrderToUpdate);
        if (!transportOrderToUpdate.getStatus().equals(TransportStatus.COMPLETE)) {
            transportOrderToUpdate.setDateEndedReal(null);
        }
        transportOrderToUpdate.setStatus(newTransportStatus);

        OrderHistory orderHistory = OrderHistory.builder()
                .transportOrder(transportOrderToUpdate)
                .changedBy(transportOrderToUpdate.getSupervisor())
                .transportStatus(newTransportStatus)
                .build();

        orderHistoryRepository.save(orderHistory);
        transportOrderRepository.save(transportOrderToUpdate);

        if (newTransportStatus == TransportStatus.COMPLETE) {
            invoiceService.generateInvoice(transportOrderToUpdate);
        }
    }

    public void updateSupervisor(Long orderId, UserDetails details) {
        TransportOrder transportOrder = transportOrderRepository.findById(orderId).orElseThrow(OrderNotFoundException::new);
        Employee exSupervisor = transportOrder.getSupervisor();

        if (details.getUsername().equals(exSupervisor.getEmail())) {
            throw new SupervisorAlreadyAssigned();
        }

        Employee currentSupervisor = employeeRepository.findByEmail(details.getUsername()).orElseThrow(EmployeeNotFoundException::new);
        transportOrder.setSupervisor(currentSupervisor);
        transportOrderRepository.save(transportOrder);
    }

    public void update(TransportOrderDto transportOrderDto, UserDetails userDetails) {
        TransportOrder transportOrderToUpdate = transportOrderRepository.findById(transportOrderDto.getId()).orElseThrow(OrderNotFoundException::new);

        orderValidationService.validateSupervisor(userDetails, transportOrderToUpdate);

        Cargo cargoTouUpdate = transportOrderToUpdate.getCargo();

        cargoTouUpdate.setCargoType(transportOrderDto.getCargoDto().getCargoType());
        cargoTouUpdate.setDescription(transportOrderDto.getCargoDto().getDescription());
        cargoTouUpdate.setQuantity(transportOrderDto.getCargoDto().getQuantity());
        cargoTouUpdate.setQuantityType(transportOrderDto.getCargoDto().getQuantityType());

        transportOrderToUpdate.setCargo(cargoTouUpdate);
        transportOrderToUpdate.setCurrency(transportOrderDto.getCurrency());
        transportOrderToUpdate.setAddressTo(transportOrderDto.getAddressTo());
        transportOrderToUpdate.setAddressFrom(transportOrderDto.getAddressFrom());
        transportOrderToUpdate.setDateStartedPlanned(transportOrderDto.getDateStartedPlanned());
        transportOrderToUpdate.setDateStartedReal(transportOrderDto.getDateStartedReal());
        transportOrderToUpdate.setDateEndedPlanned(transportOrderDto.getDateEndedPlanned());
        transportOrderToUpdate.setCarrierFee(transportOrderDto.getCarrierFee());
        transportOrderToUpdate.setTotalPrice(transportOrderDto.getTotalPrice());
        transportOrderToUpdate.setVehicleNumber(transportOrderDto.getVehicleNumber());

        orderValidationService.validateRealDates(transportOrderToUpdate);

        transportOrderRepository.save(transportOrderToUpdate);
    }

    public void deleteOrder(Long orderId, UserDetails userDetails) {
        TransportOrder transportOrderToDelete = transportOrderRepository.findById(orderId).orElseThrow(OrderNotFoundException::new);

        orderValidationService.validateSupervisor(userDetails, transportOrderToDelete);

        orderValidationService.deleteStatusValidator(transportOrderToDelete);

        transportOrderRepository.deleteById(transportOrderToDelete.getId());
    }

    public void completeOrder(Long orderId, LocalDate actualDeliveryDate, UserDetails details) {
        TransportOrder transportOrder = transportOrderRepository.findById(orderId).orElseThrow(OrderNotFoundException::new);
        orderValidationService.validateSupervisor(details, transportOrder);

        orderValidationService.deliveryDateValidator(actualDeliveryDate, transportOrder);
        transportOrder.setStatus(TransportStatus.COMPLETE);

        transportOrderRepository.save(transportOrder);
    }

    @Transactional
    public List<TransportOrder> saveBatch(MultipartFile file, UserDetails userDetails) {
        List<TransportOrderCsvDTO> ordersCsv = readCsvFile(file);
        Employee supervisor = employeeRepository.findByEmail(userDetails.getUsername()).orElseThrow(EmployeeNotFoundException::new);
        Map<Long, Carrier> carrierCache = getCarrierCache(ordersCsv);
        Map<Long, Client> clientCache = getClientCache(ordersCsv);

        List<TransportOrder> orders = ordersCsv.stream()
                .map(csvData -> TransportOrderCsvDTO.toOrderBuilder(csvData)
                        .carrier(carrierCache.get(csvData.getCarrierId()))
                        .client(clientCache.get(csvData.getClientId()))
                        .supervisor(supervisor)
                        .build()
                ).collect(Collectors.toList());
        transportOrderRepository.saveAll(orders);
        saveBatchHistory(orders);

        return orders;
    }

    private Map<Long, Client> getClientCache(List<TransportOrderCsvDTO> orderCsv) {
        Set<Long> clientIds = orderCsv.stream()
                .map(TransportOrderCsvDTO::getClientId)
                .collect(Collectors.toSet());
        Map<Long, Client> clientCache = clientRepository.findByIdIn(clientIds)
                .stream()
                .collect(Collectors.toMap(
                        client -> client.getId(),
                        client -> client
                ));

        if (clientIds.size() != clientCache.values().size()) {
            throw new CsvException("Incorrect client id");
        }

        return clientCache;
    }

    private Map<Long, Carrier> getCarrierCache(List<TransportOrderCsvDTO> ordersCsv) {
        Set<Long> carriersIds = ordersCsv.stream()
                .map(TransportOrderCsvDTO::getCarrierId)
                .collect(Collectors.toSet());
        Map<Long, Carrier> carrierCache = carrierRepository.findByIdIn(carriersIds)
                .stream()
                .collect(Collectors.toMap(
                        carrier -> carrier.getId(),
                        carrier -> carrier
                ));
        if (carriersIds.size() != carrierCache.values().size()) {
            throw new CsvException("Incorrect carrier id");
        }
        return carrierCache;
    }

    void saveBatchHistory(List<TransportOrder> orders) {
        List<OrderHistory> list = orders.stream()
                .map(order -> OrderHistory.builder()
                        .transportOrder(order)
                        .transportStatus(order.getStatus())
                        .changedBy(order.getSupervisor())
                        .build()).collect(Collectors.toList());

        orderHistoryRepository.saveAll(list);
    }

    private List<TransportOrderCsvDTO> readCsvFile(MultipartFile file) {
        try (Reader reader = new InputStreamReader(file.getInputStream())) {
            return new CsvToBeanBuilder<TransportOrderCsvDTO>(reader)
                    .withType(TransportOrderCsvDTO.class)
                    .withSeparator(',')
                    .build()
                    .parse();
        } catch (IOException e) {
            throw new CsvException(e);
        }
    }
}
