package com.lukash.logistics.feature.transport.model;

import com.lukash.logistics.feature.transport.dto.CargoDto;
import com.lukash.logistics.feature.transport.model.enums.QuantityType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cargo {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String cargoType;

    @Column(nullable = false)
    @Min(0)
    private double quantity;

    @Column(nullable = false)
    private QuantityType quantityType;

    public static Cargo ofDto(CargoDto cargoDto) {
        return Cargo.builder()
                .description(cargoDto.getDescription())
                .cargoType(cargoDto.getCargoType())
                .quantity(cargoDto.getQuantity())
                .quantityType(cargoDto.getQuantityType())
                .build();
    }
}
