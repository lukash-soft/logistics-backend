package com.lukash.logistics.feature.transport.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class CsvException extends ApplicationException {
    public CsvException(final String message) {
        super(message);
    }
    public CsvException(final Throwable err) {
        super(err);
    }
}
