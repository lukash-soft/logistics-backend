package com.lukash.logistics.feature.transport.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class OrderAuthorizationException extends ApplicationException {
    public OrderAuthorizationException() {
        super("User is not authorized to modify the order");
    }
}
