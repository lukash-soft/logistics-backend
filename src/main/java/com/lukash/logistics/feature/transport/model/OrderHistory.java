package com.lukash.logistics.feature.transport.model;

import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import com.lukash.logistics.feature.user.employee.model.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderHistory {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false)
    private TransportOrder transportOrder;

    @Column(nullable = false)
    private TransportStatus transportStatus;

    @Column(nullable = false)
    @Builder.Default
    private LocalDate dateChanged = LocalDate.now();

    @ManyToOne(optional = false)
    private Employee changedBy;
}
