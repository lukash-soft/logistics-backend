package com.lukash.logistics.feature.transport.dto;

import com.lukash.logistics.feature.transport.model.Cargo;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.model.enums.CurrencyModel;
import com.lukash.logistics.feature.transport.model.enums.QuantityType;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import com.opencsv.bean.CsvNumber;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransportOrderCsvDTO {

    @CsvBindByName(column = "CARRIER")
    @CsvNumber("#")
    private Long carrierId;
    @CsvBindByName(column = "FROM")
    private String addressFrom;
    @CsvBindByName(column = "TO")
    private String addressTo;
    @CsvBindByName(column = "START")
    @CsvDate("dd.MM.yyyy")
    private LocalDate dateStartedPlanned;
    @CsvBindByName(column = "END")
    @CsvDate("dd.MM.yyyy")
    private LocalDate dateEndedPlanned;
    @CsvBindByName(column = "FEE")
    @CsvNumber(value = "#,##")
    private double carrierFee;
    @CsvBindByName(column = "PRICE")
    @CsvNumber(value = "#,##")
    private double totalPrice;
    @CsvBindByName(column = "CURRENCY")
    @Builder.Default
    private CurrencyModel currency = CurrencyModel.PLN;
    @CsvBindByName(column = "VEHICLE_NUMBER")
    private String vehicleNumber;
    @CsvBindByName(column = "CLIENT")
    private Long clientId;
    @CsvBindByName(column = "CARGO_DESCRIPTION")
    private String cargoDescription;
    @CsvBindByName(column = "CARGO_TYPE")
    private String cargoType;
    @CsvBindByName(column = "QUANTITY")
    @CsvNumber(value = "#.##")
    private double quantity;
    @CsvBindByName(column = "QUANTITY_TYPE")
    private QuantityType quantityType;

    public static TransportOrder.TransportOrderBuilder toOrderBuilder(TransportOrderCsvDTO transportOrderCsvDTO) {
        Cargo cargo = Cargo.builder()
                .description(transportOrderCsvDTO.getCargoDescription())
                .cargoType(transportOrderCsvDTO.getCargoType())
                .quantity(transportOrderCsvDTO.getQuantity())
                .quantityType(transportOrderCsvDTO.getQuantityType())
                .build();

        return TransportOrder.builder()
                .addressFrom(transportOrderCsvDTO.getAddressFrom())
                .addressTo(transportOrderCsvDTO.getAddressTo())
                .dateStartedPlanned(transportOrderCsvDTO.getDateStartedPlanned())
                .dateEndedPlanned(transportOrderCsvDTO.getDateEndedPlanned())
                .carrierFee(transportOrderCsvDTO.getCarrierFee())
                .totalPrice(transportOrderCsvDTO.getTotalPrice())
                .vehicleNumber(transportOrderCsvDTO.getVehicleNumber())
                .cargo(cargo);
    }
}
