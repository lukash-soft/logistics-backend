package com.lukash.logistics.feature.transport.model.enums;

public enum QuantityType {
    KILOGRAMS, PIECES
}
