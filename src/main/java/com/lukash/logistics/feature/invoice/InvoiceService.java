package com.lukash.logistics.feature.invoice;

import com.lukash.logistics.configuration.security.service.UserService;
import com.lukash.logistics.feature.company.service.CompanyService;
import com.lukash.logistics.feature.invoice.exception.DuplicatedInvoiceException;
import com.lukash.logistics.feature.transport.exception.OrderNotFoundException;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class InvoiceService {

    private final InvoiceRepository invoiceRepository;
    private final CompanyService companyService;
    private final TransportOrderRepository transportOrderRepository;
    private final UserService userService;


    public void generateInvoice(Long orderId) {
        TransportOrder order = transportOrderRepository.findById(orderId).orElseThrow(OrderNotFoundException::new);

        generateInvoice(order);
    }

    public void generateInvoice(TransportOrder transportOrder) {
        validateIfInvoiceAlreadyExists(transportOrder);

        companyService.get().ifPresent(value -> {
                    invoiceRepository.save(Invoice.builder()
                            .transportOrder(transportOrder)
                            .clientName(transportOrder.getClient().getCompanyName())
                            .totalPrice(transportOrder.getTotalPrice())
                            .accountNumber(value.getAccountNumber())
                            .paymentDeadline(LocalDate.now().plusDays(7))
                            .build());
                }
        );
    }

    private void validateIfInvoiceAlreadyExists(TransportOrder transportOrder) {
        invoiceRepository.findByTransportOrder(transportOrder)
                .ifPresent(value -> {
                    throw new DuplicatedInvoiceException();
                });
    }
}
