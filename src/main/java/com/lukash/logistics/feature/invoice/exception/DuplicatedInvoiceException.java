package com.lukash.logistics.feature.invoice.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class DuplicatedInvoiceException extends ApplicationException {
    public DuplicatedInvoiceException() {
        super("Invoice for this order already exists");
    }
}
