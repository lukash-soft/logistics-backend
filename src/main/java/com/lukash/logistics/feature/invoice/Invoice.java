package com.lukash.logistics.feature.invoice;

import com.lukash.logistics.feature.transport.model.TransportOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Invoice {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    @Builder.Default
    private LocalDate created = LocalDate.now();
    @Column(nullable = false)
    private LocalDate paymentDeadline;
    @Column
    @Builder.Default
    private boolean isPayed = false;
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private TransportOrder transportOrder;
    @Column(nullable = false)
    @Min(0)
    private double totalPrice;
    @Column(nullable = false)
    private String clientName;
    @Column(nullable = false)
    private String accountNumber;

}
