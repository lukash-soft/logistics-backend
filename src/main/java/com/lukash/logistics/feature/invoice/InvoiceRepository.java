package com.lukash.logistics.feature.invoice;

import com.lukash.logistics.feature.transport.model.TransportOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
    Optional<Invoice> findByTransportOrder(TransportOrder transportOrder);
}
