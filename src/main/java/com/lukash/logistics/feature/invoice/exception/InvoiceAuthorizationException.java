package com.lukash.logistics.feature.invoice.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class InvoiceAuthorizationException extends ApplicationException {
    public InvoiceAuthorizationException() {
        super("You are not a supervisor, so you are not authorized to generate invoice.");
    }
}
