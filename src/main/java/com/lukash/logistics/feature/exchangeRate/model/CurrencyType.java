package com.lukash.logistics.feature.exchangeRate.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CurrencyType {
    USD, PLN, EUR;

}
