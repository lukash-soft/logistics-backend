package com.lukash.logistics.feature.exchangeRate.service;

import com.lukash.logistics.feature.exchangeRate.dto.ExchangeRateResponse;
import com.lukash.logistics.feature.exchangeRate.model.CurrencyType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

@Service
public class ExchangeRateService {
    private final RestTemplate restTemplate;
    private final String baseUrl;

    public ExchangeRateService(RestTemplate restTemplate,
                               @Value("${exchange.api}") String baseUrl) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    public double convertToPln(double sourceValue, CurrencyType currencyCode, LocalDate dateOfExchangeRate) {
        String url = String.format("%sc/%s/%s/?format=json", baseUrl, currencyCode, dateOfExchangeRate);

        ExchangeRateResponse response = restTemplate.getForObject(url, ExchangeRateResponse.class);

        String ask = response.getRates().get(0).getAsk();

        return Double.parseDouble(ask) * sourceValue;
    }

}
