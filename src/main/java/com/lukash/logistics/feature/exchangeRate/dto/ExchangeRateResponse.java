package com.lukash.logistics.feature.exchangeRate.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class ExchangeRateResponse {
    private String code;
    private List<RateDto> rates;
}
