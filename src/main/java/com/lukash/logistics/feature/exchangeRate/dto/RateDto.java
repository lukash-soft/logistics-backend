package com.lukash.logistics.feature.exchangeRate.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RateDto {
    private String effectiveDate;
    private String bid;
    private String ask;
}
