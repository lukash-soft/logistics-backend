package com.lukash.logistics.feature.damageReport.model;

import com.lukash.logistics.feature.files.model.TransportOrderFile;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.user.employee.model.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DamageReport {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String description;
    @OneToOne(optional = false)
    private TransportOrder transportOrder;
    @ManyToOne(optional = false)
    private Employee supervisor;
    @OneToMany
    @Builder.Default
    private List<TransportOrderFile> orderFile = new ArrayList<>();
}
