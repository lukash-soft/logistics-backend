package com.lukash.logistics.feature.damageReport.exception;

import com.lukash.logistics.configuration.ApplicationException;

public class DamageReportNotFoundException extends ApplicationException {
    public DamageReportNotFoundException() {
        super("Damage report not found");
    }
}
