package com.lukash.logistics.feature.damageReport.controller;

import com.lukash.logistics.feature.damageReport.dto.DamageReportDto;
import com.lukash.logistics.feature.damageReport.dto.DamageReportUpdateDto;
import com.lukash.logistics.feature.damageReport.service.DamageReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/damageReports")
@RequiredArgsConstructor
public class DamageReportController {

    private final DamageReportService damageReportService;

    @GetMapping("orderId/{orderId}")
    public DamageReportDto getByOrderId(@PathVariable Long orderId) {
        return DamageReportDto.of(damageReportService.getReportByOrderId(orderId));
    }

    @PostMapping("orderId/{orderId}")
    public void saveDamageReport(
            @PathVariable Long orderId,
            @RequestBody DamageReportDto damageReportDto,
            @RequestPart("file") final MultipartFile file,
            @AuthenticationPrincipal UserDetails userDetails) {
        damageReportService.save(orderId, damageReportDto, file, userDetails);
    }

    @PutMapping("orderId/{orderId}")
    public void update(
            @PathVariable Long orderId,
            @RequestBody DamageReportUpdateDto damageReportUpdateDto,
            @AuthenticationPrincipal UserDetails userDetails) {
        damageReportService.update(orderId, damageReportUpdateDto, userDetails);
    }

    @DeleteMapping("orderId/{orderId}")
    public void delete(
            @PathVariable Long orderId,
            @AuthenticationPrincipal UserDetails userDetails) {
        damageReportService.delete(orderId, userDetails);
    }

}
