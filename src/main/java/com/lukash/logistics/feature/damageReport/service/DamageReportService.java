package com.lukash.logistics.feature.damageReport.service;

import com.lukash.logistics.feature.damageReport.dto.DamageReportDto;
import com.lukash.logistics.feature.damageReport.dto.DamageReportUpdateDto;
import com.lukash.logistics.feature.damageReport.exception.DamageReportNotFoundException;
import com.lukash.logistics.feature.damageReport.model.DamageReport;
import com.lukash.logistics.feature.damageReport.repository.DamageReportRepository;
import com.lukash.logistics.feature.files.dto.AddDamageFileDto;
import com.lukash.logistics.feature.files.model.TransportOrderFile;
import com.lukash.logistics.feature.files.service.TransportOrderFileService;
import com.lukash.logistics.feature.transport.exception.OrderNotFoundException;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.employee.exception.EmployeeNotAllowedException;
import com.lukash.logistics.feature.user.employee.exception.EmployeeNotFoundException;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DamageReportService {

    private final DamageReportRepository damageReportRepository;
    private final TransportOrderRepository transportOrderRepository;
    private final EmployeeRepository employeeRepository;
    private final TransportOrderFileService transportOrderFileService;

    public DamageReport getReportByOrderId(Long orderId) {

        return damageReportRepository.findByTransportOrderId(orderId).orElseThrow(DamageReportNotFoundException::new);
    }


    @Transactional
    public void save(Long orderId, DamageReportDto damageReportDto, MultipartFile file, UserDetails userDetails) {
        final Employee reportedBy = employeeRepository.findByEmail(userDetails.getUsername()).orElseThrow(EmployeeNotFoundException::new);
        final TransportOrder transportOrder = transportOrderRepository.findByIdAndSupervisor(orderId, reportedBy).orElseThrow(OrderNotFoundException::new);
        AddDamageFileDto addDamageFileDto = AddDamageFileDto.builder()
                .orderId(transportOrder.getId())
                .description(damageReportDto.getDescription())
                .build();

        final List<TransportOrderFile> orderFiles = createDamageFiles(addDamageFileDto, file, userDetails);

        DamageReport damageReportToSave = DamageReport.builder()
                .supervisor(reportedBy)
                .description(damageReportDto.getDescription())
                .transportOrder(transportOrder)
                .orderFile(orderFiles)
                .build();

        damageReportRepository.save(damageReportToSave);
    }

    private List<TransportOrderFile> createDamageFiles(AddDamageFileDto addDamageFileDto, MultipartFile file, UserDetails userDetails) {
        return Optional.ofNullable(file)
                .map(fileToSave -> {
                    final var saved = transportOrderFileService.addDamageFile(addDamageFileDto, file, userDetails);
                    return List.of(saved);
                }).orElse(List.of());
    }


    public void update(Long orderId, DamageReportUpdateDto damageReportUpdateDto, UserDetails userDetails) {
        Employee supervisor = employeeRepository.findByEmail(userDetails.getUsername()).orElseThrow(EmployeeNotAllowedException::new);

        DamageReport damageReportToUpdate = damageReportRepository.findByTransportOrderIdAndSupervisor(orderId, supervisor)
                .orElseThrow(DamageReportNotFoundException::new);

        damageReportToUpdate.setDescription(damageReportUpdateDto.getDescription());

        damageReportRepository.save(damageReportToUpdate);
    }

    public void delete(Long orderId, UserDetails userDetails) {
        final Employee supervisor = employeeRepository.findByEmail(userDetails.getUsername()).orElseThrow(EmployeeNotFoundException::new);

        DamageReport damageReportToDelete = damageReportRepository.findByTransportOrderIdAndSupervisor(orderId, supervisor)
                .orElseThrow(DamageReportNotFoundException::new);

        damageReportRepository.deleteById(damageReportToDelete.getId());
    }

}
