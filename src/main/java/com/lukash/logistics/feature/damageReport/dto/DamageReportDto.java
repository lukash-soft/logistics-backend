package com.lukash.logistics.feature.damageReport.dto;

import com.lukash.logistics.feature.damageReport.model.DamageReport;
import com.lukash.logistics.feature.files.dto.TransportOrderFileDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DamageReportDto {

    private Long id;
    private String description;
    private Long reportedById;
    private List<TransportOrderFileDto> fileSummaryList;

    public static DamageReportDto of(DamageReport damageReport) {
        return DamageReportDto.builder()
                .id(damageReport.getId())
                .description(damageReport.getDescription())
                .reportedById(damageReport.getSupervisor().getId())
                .fileSummaryList(TransportOrderFileDto.ofList(damageReport.getOrderFile()))
                .build();
    }
}
