package com.lukash.logistics.feature.damageReport.repository;

import com.lukash.logistics.feature.damageReport.model.DamageReport;
import com.lukash.logistics.feature.user.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DamageReportRepository extends JpaRepository<DamageReport, Long> {
    Optional<DamageReport> findByTransportOrderId(Long orderId);

    Optional<DamageReport> findByTransportOrderIdAndSupervisor(Long orderId, Employee supervisor);
}
