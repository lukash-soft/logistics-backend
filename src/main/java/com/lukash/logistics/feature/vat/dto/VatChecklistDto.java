package com.lukash.logistics.feature.vat.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VatChecklistDto {
    private Map<String, Boolean> vatNumberMap;
}
