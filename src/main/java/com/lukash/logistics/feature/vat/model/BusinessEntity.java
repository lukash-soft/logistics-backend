package com.lukash.logistics.feature.vat.model;

public interface BusinessEntity {
    String getCompanyName();

    String getTaxNumber();

    String getAccountNumber();
}
