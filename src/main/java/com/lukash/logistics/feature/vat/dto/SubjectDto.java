package com.lukash.logistics.feature.vat.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubjectDto {

    @JsonProperty(value = "nip")
    private String vatId;
    private String name;
    private List<String> accountNumbers;
}
