package com.lukash.logistics.feature.vat.service;

import com.lukash.logistics.feature.vat.dto.EntryDto;
import com.lukash.logistics.feature.vat.dto.VatChecklistDto;
import com.lukash.logistics.feature.vat.dto.WhitelistMultipleResultsDto;
import com.lukash.logistics.feature.vat.model.BusinessEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class VatCheckService {
    private final RestTemplate restTemplate;
    private final String baseUrl;

    public VatCheckService(RestTemplate restTemplate,
                           @Value("${vat.api}") String baseUrl) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    public VatChecklistDto areRegisteredEntities(List<BusinessEntity> businessEntities) {
        WhitelistMultipleResultsDto response = getWhitelistedAccounts(businessEntities);

        final Map<String, List<String>> identifierToAccountNumbersMap = response.getResult().getEntries().stream()
                .collect(Collectors.toUnmodifiableMap(
                        EntryDto::getIdentifier,
                        entryDto -> entryDto.getSubjects().stream()
                                .flatMap(subject -> subject.getAccountNumbers().stream())
                                .collect(Collectors.toList())
                ));

        Map<String, Boolean> mapResult = businessEntities.stream()
                .collect(Collectors.toUnmodifiableMap(
                        BusinessEntity::getTaxNumber,
                        entity -> identifierToAccountNumbersMap.get(entity.getTaxNumber()).contains(entity.getAccountNumber())
                ));

        return VatChecklistDto.builder()
                .vatNumberMap(mapResult)
                .build();
    }

    private WhitelistMultipleResultsDto getWhitelistedAccounts(List<BusinessEntity> businessEntities) {
        String url = getUrl(businessEntities);

        WhitelistMultipleResultsDto response = restTemplate.getForObject(url, WhitelistMultipleResultsDto.class);
        return response;
    }

    private String getUrl(List<BusinessEntity> businessEntities) {
        String vatNumbersJoined = businessEntities.stream()
                .map(BusinessEntity::getTaxNumber)
                .collect(Collectors.joining(","));
        return baseUrl + vatNumbersJoined + "?date=" + LocalDate.now();
    }
}
