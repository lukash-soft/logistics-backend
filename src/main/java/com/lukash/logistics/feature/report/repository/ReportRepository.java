package com.lukash.logistics.feature.report.repository;

import com.lukash.logistics.feature.report.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ReportRepository extends JpaRepository<Report, Long> {
    List<Report> findAllByForPeriod(LocalDate forPeriod);
}
