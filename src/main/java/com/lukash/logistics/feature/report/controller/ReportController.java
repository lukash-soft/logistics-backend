package com.lukash.logistics.feature.report.controller;

import com.lukash.logistics.feature.report.service.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

import static com.lukash.logistics.configuration.security.constants.UserRole.RoleValues.ROLE_MANAGER;

@RequiredArgsConstructor
@Secured({ROLE_MANAGER})
@RestController
@RequestMapping("reports")
public class ReportController {

    private final ReportService reportService;

    @GetMapping
    public void getAll(@RequestParam(required = false, defaultValue = "#{T(java.time.LocalDateTime).now().minusMonths(6))}") LocalDate from) {
        reportService.getAll(from);
    }
}
