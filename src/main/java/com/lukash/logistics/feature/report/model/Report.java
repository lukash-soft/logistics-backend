package com.lukash.logistics.feature.report.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Report {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private LocalDate forPeriod;

    @Column(nullable = false)
    private long numbersOfOrdersInProgress;

    @Column(nullable = false)
    private int numberOfOrdersInProgressDelayed;

    @Column(nullable = false)
    private int numberOfOrdersCompletedInTime;

    @Column(nullable = false)
    private int numbersOfOrdersCompletedLate;

    @Column(nullable = false)
    private int numbersOfOrdersCanceled;

    @Column(nullable = false)
    private double incomeGross;

    @Column(nullable = false)
    private double incomeNet;

    @Column(nullable = false)
    private double employeeCost;

    @Column(nullable = false)
    private double fees;

}
