package com.lukash.logistics.feature.report.service;

import com.lukash.logistics.feature.report.model.Report;
import com.lukash.logistics.feature.report.repository.ReportRepository;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReportService {

    private final ReportRepository reportRepository;
    private final TransportOrderRepository transportOrderRepository;
    private final EmployeeService employeeService;

    public List<Report> getAll(LocalDate from) {
        return reportRepository.findAllByForPeriod(from);
    }

    public Report generateReport(LocalDate reportDate) {
        //Get the range dates for orders
        LocalDate rangeStartDate = reportDate.minusDays(1);
        LocalDate rangeEndDate = reportDate.plusMonths(1);

        //Get lists of orders and employees for the report
        final List<Employee> activeEmployees = employeeService.getUsers(true);
        final List<TransportOrder> activeOrders = transportOrderRepository.findByStatusIn(TransportStatus.ACTIVE_STATUSES);
        final List<TransportOrder> completedOrders = transportOrderRepository.findByStatusAndDateEndedRealIsBetween(TransportStatus.COMPLETE, rangeStartDate, rangeEndDate);
        final List<TransportOrder> cancelledOrders = transportOrderRepository.findByStatusAndDateEndedRealIsBetween(TransportStatus.CANCELLED, rangeStartDate, rangeEndDate);

        //Get money fields for the report
        double incomeGross = calculateIncomeGross(completedOrders);
        double fees = calculateFees(completedOrders);
        double employeeCost = calculateEmployeeCost(activeEmployees);
        double incomeNet = incomeGross - fees - employeeCost;

        //Get number of orders necessary for the report
        int activeOrdersInProgress = activeOrders.size();
        int ordersCompletedInTime = countOrdersCompletedInTime(completedOrders);
        int ordersInProgressDelayed = countOrdersInProgressDelayed(LocalDate.now(), activeOrders);
        int ordersCompletedDelayed = countOrdersCompletedDelayed(completedOrders);
        int ordersCancelled = cancelledOrders.size();

        //Generate report
        return Report.builder()
                .forPeriod(reportDate)
                .numbersOfOrdersInProgress(activeOrdersInProgress)
                .numberOfOrdersInProgressDelayed(ordersInProgressDelayed)
                .numberOfOrdersCompletedInTime(ordersCompletedInTime)
                .numbersOfOrdersCompletedLate(ordersCompletedDelayed)
                .numbersOfOrdersCanceled(ordersCancelled)
                .incomeGross(incomeGross)
                .employeeCost(employeeCost)
                .fees(fees)
                .incomeNet(incomeNet)
                .build();
    }

    //We can extract all methods below to a separate class.
    private static int countOrdersCompletedDelayed(List<TransportOrder> completedOrders) {
        return (int) completedOrders.stream()
                .filter(order -> order.getDateEndedReal().isAfter(order.getDateEndedPlanned()))
                .count();
    }

    private static int countOrdersInProgressDelayed(LocalDate today, List<TransportOrder> activeOrders) {
        return (int) activeOrders.stream()
                .filter(transportOrder -> transportOrder.getDateEndedReal() == null)
                .filter(transportOrder -> today.isAfter(transportOrder.getDateEndedPlanned()))
                .count();
    }

    private static int countOrdersCompletedInTime(List<TransportOrder> completedOrders) {
        return (int) completedOrders.stream().filter(order -> order.getDateEndedReal().equals(order.getDateEndedPlanned())).count();
    }

    private static double calculateEmployeeCost(List<Employee> activeEmployees) {
        return activeEmployees.stream().mapToDouble(Employee::getSalary).sum();
    }

    private static double calculateFees(List<TransportOrder> completedOrders) {
        return completedOrders.stream().mapToDouble(TransportOrder::getCarrierFee).sum();
    }

    private static double calculateIncomeGross(List<TransportOrder> completedOrders) {
        return completedOrders.stream().mapToDouble(TransportOrder::getTotalPrice).sum();
    }
}
