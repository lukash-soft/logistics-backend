package com.lukash.logistics.configuration;

public class ApplicationException extends RuntimeException {
    public ApplicationException(final String message) {
        super(message);
    }

    public ApplicationException(final Throwable cause) {
        super(cause);
    }
}
