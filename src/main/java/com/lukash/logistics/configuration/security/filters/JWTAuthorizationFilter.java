package com.lukash.logistics.configuration.security.filters;

import com.lukash.logistics.configuration.security.model.JwtTokenValues;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.lukash.logistics.configuration.security.constants.SecurityConstants.HEADER_STRING;
import static com.lukash.logistics.configuration.security.constants.SecurityConstants.TOKEN_PREFIX;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
    public JWTAuthorizationFilter(final AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest req,
                                    final HttpServletResponse res,
                                    final FilterChain chain) throws IOException, ServletException {
        Optional.ofNullable(req.getHeader(HEADER_STRING))
                .filter(header -> header.startsWith(TOKEN_PREFIX))
                .ifPresent(this::getAuthentication);

        chain.doFilter(req, res);
    }

    private void getAuthentication(final String token) {
        final JwtTokenValues tokenValues = JwtTokenValues.decodeJwt(token);
        final User principal = new User(tokenValues.getUsername(), "", Collections.emptyList());

        final var authentication = new UsernamePasswordAuthenticationToken(principal, token, List.of(new SimpleGrantedAuthority(tokenValues.getRole())));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
