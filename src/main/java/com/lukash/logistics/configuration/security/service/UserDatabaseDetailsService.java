package com.lukash.logistics.configuration.security.service;

import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserDatabaseDetailsService implements UserDetailsService {

    private final EmployeeRepository employeeRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        final Employee employee = employeeRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException(email));
        final List<GrantedAuthority> permissions = List.of(new SimpleGrantedAuthority(employee.getRole().value));

        final boolean isActive = employee.isAccountActive();

        return new User(
                employee.getEmail(),
                employee.getPassword(),
                true,
                isActive,
                true,
                true,
                permissions);
    }
}
