package com.lukash.logistics.configuration.security.service;

import com.lukash.logistics.configuration.security.exception.AuthException;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final EmployeeRepository accountRepository;

    public Employee getLoggedAccount() {
        final String email = getUserPrincipal().getUsername();
        return accountRepository.findByEmail(email)
                .orElseThrow(() -> new AuthException("Could not find logged user"));
    }

    private User getUserPrincipal() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (User) authentication.getPrincipal();
    }
}
