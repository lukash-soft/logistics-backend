package com.lukash.logistics.configuration.security.model;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.lukash.logistics.configuration.security.constants.SecurityConstants.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtTokenValues {

    private String username;
    private String role;

    public static JwtTokenValues decodeJwt(final String token) {
        final DecodedJWT decodedJWT = decode(token);
        final String username = decodedJWT.getSubject();
        final String role = decodedJWT.getClaim(ROLE_KEY).asString();

        return new JwtTokenValues(username, role);
    }

    private static DecodedJWT decode(final String token) {
        return JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""));
    }
}
