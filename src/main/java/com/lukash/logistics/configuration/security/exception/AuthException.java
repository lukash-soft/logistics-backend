package com.lukash.logistics.configuration.security.exception;

import com.lukash.logistics.configuration.ApplicationException;


public class AuthException extends ApplicationException {

    public AuthException(final String message) {
        super(message);
    }
}
