package com.lukash.logistics.configuration.security.constants;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserRole {

    OWNER(RoleValues.ROLE_OWNER), EMPLOYEE(RoleValues.ROLE_EMPLOYEE), MANAGER(RoleValues.ROLE_MANAGER), ADMIN(RoleValues.ROLE_ADMIN);

    public final String value;

    public interface RoleValues {
        String ROLE_OWNER = "ROLE_OWNER";
        String ROLE_EMPLOYEE = "ROLE_EMPLOYEE";
        String ROLE_MANAGER = "ROLE_MANAGER";
        String ROLE_ADMIN = "ROLE_ADMIN";
    }
}
