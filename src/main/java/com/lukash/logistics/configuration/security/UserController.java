package com.lukash.logistics.configuration.security;

import com.lukash.logistics.feature.user.employee.dto.PasswordChangeDto;
import com.lukash.logistics.feature.user.employee.service.UserPasswordService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("passwords")
@RequiredArgsConstructor
public class UserController  {

    private final UserPasswordService userPasswordService;

    @PutMapping("reset")
    public void resetPassword(@RequestBody String email) {
        userPasswordService.resetPassword(email);
    }

    @PutMapping("update")
    public void updatePassword(@RequestBody PasswordChangeDto passwordChangeDto) {
        userPasswordService.updatePassword(passwordChangeDto);
    }
}
