package com.lukash.logistics.feature.user.employee.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.configuration.security.exception.AuthException;
import com.lukash.logistics.feature.user.employee.dto.EmployeeRegistrationDto;
import com.lukash.logistics.feature.user.employee.dto.EmployeeUpdateNameDto;
import com.lukash.logistics.feature.user.employee.dto.EmployeeUpdateSalaryDto;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class EmployeeServiceTest extends IntegrationTest {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    EmployeeService employeeService;

    @AfterEach
    void cleanUp() {
        employeeRepository.deleteAll();
    }

    @Test
    void shouldGetAllActiveEmployees() {
        //given
        final Employee active = Fixture.prepareEmployee(UserRole.EMPLOYEE);
        employeeRepository.save(active);
        final Employee nonActive = employeeRepository.save(Employee.builder()
                .firstName("First name " + 2)
                .lastName("Last name" + 2)
                .email("testEmial")
                .role(UserRole.EMPLOYEE)
                .salary(2000.1)
                .password("pass")
                .dateStarted(LocalDate.now().minusYears(1))
                .dateEnded(LocalDate.now().minusMonths(1))
                .build());
        employeeRepository.saveAll(List.of(active, nonActive));
        assertThat(employeeRepository.findAll()).hasSize(2);

        //when
        final List<Employee> result = employeeService.getUsers(true);

        //then
        assertThat(result).extracting(Employee::getId).containsExactlyInAnyOrder(active.getId());
    }

    @Test
    void shouldGetAllRegardlessOfStatus() {
        //given
        final Employee active = Fixture.prepareEmployee(UserRole.EMPLOYEE);
        employeeRepository.save(active);

        final Employee nonActive = employeeRepository.save(Employee.builder()
                .firstName("First name " + 2)
                .lastName("Last name" + 2)
                .email("testEmail")
                .role(UserRole.EMPLOYEE)
                .salary(2000.1)
                .password("pass")
                .dateStarted(LocalDate.now())
                .dateEnded(LocalDate.now())
                .build());

        employeeRepository.saveAll(List.of(active, nonActive));
        assertThat(employeeRepository.findAll()).hasSize(2);

        //when
        final List<Employee> result = employeeService.getUsers(false);

        //then
        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    void shouldSave() {
        //given
        final EmployeeRegistrationDto registrationForm = Fixture.getRegistrationDto();

        //when
        Employee savedEmployee = employeeService.save(registrationForm);

        //then
        List<Employee> allEmployees = employeeRepository.findAll();
        assertThat(allEmployees).contains(savedEmployee);
    }

    @Test
    void shouldThrowExceptionWhenSavingOwner() {
        //given
        final EmployeeRegistrationDto registrationForm = Fixture.getRegistrationDto();
        registrationForm.setRole(UserRole.OWNER);

        //when, then
        assertThatThrownBy(() -> employeeService.save(registrationForm)).isInstanceOf(AuthException.class);
    }

    @Test
    void shouldUpdateName() {
        //given
        Employee originalUser = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        EmployeeUpdateNameDto employeeUpdateNameDto = EmployeeUpdateNameDto.builder()
                .id(originalUser.getId())
                .firstName("Updated first")
                .lastName("Updated last")
                .build();

        //when
        employeeService.updateName(employeeUpdateNameDto);

        //then
        Employee updatedOriginalUser = employeeRepository.findById(originalUser.getId()).orElseThrow();

        assertThat(updatedOriginalUser.getFirstName()).isEqualTo(employeeUpdateNameDto.getFirstName());
        assertThat(updatedOriginalUser.getLastName()).isEqualTo(employeeUpdateNameDto.getLastName());
    }

    @Test
    void shouldUpdateSalary() {
        //given
        Employee originalUser = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        EmployeeUpdateSalaryDto employeeUpdateSalaryDto = EmployeeUpdateSalaryDto.builder()
                .id(originalUser.getId())
                .salary(originalUser.getSalary())
                .build();

        //when
        employeeService.updateSalary(employeeUpdateSalaryDto);

        //then
        Employee updatedOriginalUser = employeeRepository.findById(originalUser.getId()).orElseThrow();
        assertThat(updatedOriginalUser.getSalary()).isEqualTo(employeeUpdateSalaryDto.getSalary());
    }

    @Test
    void shouldTerminateEmployee() {
        //given
        Employee originalUser = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        //when
        employeeService.terminate(originalUser.getId());

        //then
        Employee terminatedEmployee = employeeRepository.findById(originalUser.getId()).orElseThrow();
        assertThat(terminatedEmployee.isAccountActive()).isFalse();
    }

    @Test
    void shouldThrowExceptionWhenTerminateOwner() {
        //given
        Employee originalUser = employeeRepository.save(Fixture.prepareEmployee(UserRole.OWNER));

        //when, then
        assertThatThrownBy(() -> employeeService.terminate(originalUser.getId())).isInstanceOf(AuthException.class);
    }

    @Test
    void shouldSaveWithEncodePassword() {
        //given
        final EmployeeRegistrationDto registrationForm = EmployeeRegistrationDto.builder()
                .firstName("Test first")
                .lastName("Test lastName")
                .email("testEmail.com")
                .password("password")
                .role(UserRole.EMPLOYEE)
                .dateStarted(LocalDate.now())
                .salary(1)
                .build();

        //when
        employeeService.save(registrationForm);

        //then
        final List<Employee> allEmployees = employeeRepository.findAll();
        assertThat(allEmployees).hasSize(1);

        final Employee saved = allEmployees.get(0);
        assertThat(saved.getPassword()).isNotEqualTo(registrationForm.getPassword());
    }
}
