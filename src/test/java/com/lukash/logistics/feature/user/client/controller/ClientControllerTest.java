package com.lukash.logistics.feature.user.client.controller;

import com.lukash.logistics.feature.user.client.dto.ClientDto;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.setup.DatabaseFixture;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import com.lukash.logistics.setup.MockingEmployeeOne;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ClientControllerTest extends IntegrationTest {

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    DatabaseFixture databaseFixture;

    @BeforeEach
    void initData() {
        databaseFixture.prepareCompany();
    }

    @AfterEach
    void cleanup() {
        databaseFixture.cleanup();
    }

    @Test
    @MockingEmployeeOne
    void shouldGetActiveClients() throws Exception {
        // given
        clientRepository.save(Fixture.prepareClient("disabled", UserStatus.DISABLED));
        // when, then
        getRequest("/clients")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(DatabaseFixture.getClientsSize())));
    }

    @Test
    @MockingEmployeeOne
    void shouldSaveNewClient() throws Exception {
        // given
        final ClientDto dto = ClientDto.of(Fixture.prepareClient("someName", UserStatus.ACTIVE));
        // when
        postRequest("/clients", dto)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", greaterThan(0)));

        //then
        assertThat(clientRepository.count()).isEqualTo(DatabaseFixture.getClientsSize() + 1);
    }

    @Test
    @MockingEmployeeOne
    void shouldUpdateClient() throws Exception {
        // given
        final Client toUpdate = clientRepository.findAll().get(0);

        final ClientDto dto = ClientDto.of(toUpdate);
        dto.setAccountNumber("updated");

        // when
        putRequest("/clients", dto)
                .andExpect(status().isOk());

        //then
        assertThat(clientRepository.findById(toUpdate.getId()).get().getAccountNumber()).isEqualTo(dto.getAccountNumber());
    }
}