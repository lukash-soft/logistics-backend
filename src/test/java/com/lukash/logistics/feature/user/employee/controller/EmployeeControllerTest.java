package com.lukash.logistics.feature.user.employee.controller;

import com.lukash.logistics.feature.user.employee.dto.EmployeeDto;
import com.lukash.logistics.feature.user.employee.dto.EmployeeRegistrationDto;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.*;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.NestedServletException;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class EmployeeControllerTest extends IntegrationTest {

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    DatabaseFixture databaseFixture;

    @BeforeEach
    void initData() {
        databaseFixture.prepareCompany();
    }

    @AfterEach
    void cleanup() {
        databaseFixture.cleanup();
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "?activeOnly=true"})
    @MockingOwner
    @SneakyThrows
    void shouldGetActiveEmployeesIfOwner(final String requestParameter) {
        // given, when, then
        getRequest("/employees" + requestParameter)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(DatabaseFixture.getActiveEmployeesSize())));
    }

    @Test
    @MockingOwner
    @SneakyThrows
    void shouldGetAllEmployeesIfOwner() {
        // given, when, then
        getRequest("/employees?activeOnly=false")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(DatabaseFixture.getCompanySize())));
    }

    @Test
    @MockingEmployeeOne
    void shouldNotGetEmployeesIfEmployee() {
        // given, when, then
        assertThatThrownBy(() -> getRequest("/employees")).isInstanceOf(NestedServletException.class);
    }

    @Test
    @MockingOwner
    @SneakyThrows
    void shouldSaveIfOwner() {
        //given
        final EmployeeRegistrationDto dto = Fixture.getRegistrationDto();

        //when, then
        postRequest("/employees", dto)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", greaterThan(0)));
    }

    @Test
    @MockingEmployeeOne
    void shouldNotSaveIfEmployee() {
        //given
        final EmployeeRegistrationDto dto = Fixture.getRegistrationDto();

        //when, then
        assertThatThrownBy(() -> postRequest("/employees", dto)).isInstanceOf(NestedServletException.class);
    }

    @Test
    @MockingOwner
    @SneakyThrows
    void shouldUpdateNamesIfOwner() {
        //given
        final Employee employee = employeeRepository.findByEmail(DatabaseFixture.EMPLOYEE_1).get();

        final EmployeeDto employeeDto = EmployeeDto.builder()
                .id(employee.getId())
                .firstName("aaa")
                .lastName("bbb")
                .build();

        //when
        putRequest("/employees/name", employeeDto)
                .andExpect(status().isOk());

        //then
        final Employee afterUpdate = employeeRepository.findByEmail(DatabaseFixture.EMPLOYEE_1).get();
        assertThat(afterUpdate.getFirstName()).isEqualTo(employeeDto.getFirstName());
        assertThat(afterUpdate.getLastName()).isEqualTo(employeeDto.getLastName());

        assertThat(afterUpdate.getSalary()).isEqualTo(employee.getSalary());
        assertThat(afterUpdate.getDateEnded()).isEqualTo(employee.getDateEnded());
        assertThat(afterUpdate.getDateStarted()).isEqualTo(employee.getDateStarted());
    }

    @Test
    @MockingEmployeeOne
    void shouldNotUpdateNamesIfEmployee() {
        //given
        final EmployeeDto employeeDto = EmployeeDto.builder()
                .id(1L)
                .firstName("aaa")
                .lastName("bbb")
                .build();

        //when, then
        assertThatThrownBy(() -> putRequest("/employees/name", employeeDto)).isInstanceOf(NestedServletException.class);
    }

    @Test
    @MockingOwner
    @SneakyThrows
    void shouldUpdateSalaryIfOwner() {
        //given
        final Employee employee = employeeRepository.findByEmail(DatabaseFixture.EMPLOYEE_1).get();

        final EmployeeDto employeeDto = EmployeeDto.builder()
                .id(employee.getId())
                .firstName("aaa")
                .lastName("bbb")
                .salary(765.4)
                .build();

        //when
        putRequest("/employees/salary", employeeDto)
                .andExpect(status().isOk());

        //then
        final Employee afterUpdate = employeeRepository.findByEmail(DatabaseFixture.EMPLOYEE_1).get();
        assertThat(afterUpdate.getSalary()).isEqualTo(employeeDto.getSalary());

        assertThat(afterUpdate.getFirstName()).isEqualTo(employee.getFirstName());
        assertThat(afterUpdate.getLastName()).isEqualTo(employee.getLastName());
        assertThat(afterUpdate.getDateEnded()).isEqualTo(employee.getDateEnded());
        assertThat(afterUpdate.getDateStarted()).isEqualTo(employee.getDateStarted());
    }

    @Test
    @MockingEmployeeOne
    @SneakyThrows
    void shouldNotUpdateSalaryIfEmployee() {
        //given
        final Employee employee = employeeRepository.findByEmail(DatabaseFixture.EMPLOYEE_2).get();

        final EmployeeDto employeeDto = EmployeeDto.builder()
                .id(employee.getId())
                .firstName("aaa")
                .lastName("bbb")
                .salary(765.4)
                .build();

        //when, then
        assertThatThrownBy(() -> putRequest("/employees/salary", employeeDto)).isInstanceOf(NestedServletException.class);
    }


    @Test
    @MockingOwner
    void shouldTerminateEmployeeIfOwner() throws Exception {
        //given
        final Employee employee = employeeRepository.findByEmail(DatabaseFixture.EMPLOYEE_1).get();
        assertThat(employee.getDateEnded()).isNull();
        //when
        deleteRequest("/employees/id/" + employee.getId())
                .andExpect(status().isOk());

        //then
        final Employee afterTermination = employeeRepository.findByEmail(DatabaseFixture.EMPLOYEE_1).get();
        assertThat(afterTermination.getDateEnded()).isEqualTo(LocalDate.now());
    }

    //todo #32
    @Disabled("restore with #32")
    @Test
    @MockingEmployeeOne
    void shouldUpdatePassword() {
    }
}
