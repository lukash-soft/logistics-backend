package com.lukash.logistics.feature.user.carrier.controller;

import com.lukash.logistics.feature.user.carrier.dto.CarrierDto;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.setup.DatabaseFixture;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import com.lukash.logistics.setup.MockingEmployeeOne;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CarrierControllerTest extends IntegrationTest {

    @Autowired
    CarrierRepository carrierRepository;

    @Autowired
    DatabaseFixture databaseFixture;

    @BeforeEach
    void initData() {
        databaseFixture.prepareCompany();
    }

    @AfterEach
    void cleanup() {
        databaseFixture.cleanup();
    }

    @Test
    @MockingEmployeeOne
    void shouldGetActiveCarriers() throws Exception {
        // given
        final var disabled = Fixture.prepareCarrier("disabled");
        disabled.setUserStatus(UserStatus.DISABLED);

        carrierRepository.save(disabled);

        // when, then
        getRequest("/carriers")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(DatabaseFixture.getCarriersSize())));
    }

    @Test
    @MockingEmployeeOne
    void shouldSaveNewCarrier() throws Exception {
        // given
        final CarrierDto dto = CarrierDto.of(Fixture.prepareCarrier("someName"));
        // when
        postRequest("/carriers", dto)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", greaterThan(0)));

        //then
        assertThat(carrierRepository.count()).isEqualTo(DatabaseFixture.getCarriersSize() + 1);
    }

    @Test
    @MockingEmployeeOne
    void shouldUpdateClient() throws Exception {
        // given
        final Carrier toUpdate = carrierRepository.findAll().get(0);

        final CarrierDto dto = CarrierDto.of(toUpdate);
        dto.setAccountNumber("updated");

        // when
        putRequest("/carriers", dto)
                .andExpect(status().isOk());

        //then
        assertThat(carrierRepository.findById(toUpdate.getId()).get().getAccountNumber()).isEqualTo(dto.getAccountNumber());
    }
}