package com.lukash.logistics.feature.user.carrier.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.transport.repository.CargoRepository;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.carrier.dto.CarrierDto;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CarrierServiceTest extends IntegrationTest {

    @Autowired
    CarrierRepository carrierRepository;
    @Autowired
    CarrierService carrierService;
    @Autowired
    TransportOrderRepository transportOrderRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    CargoRepository cargoRepository;
    @Autowired
    EmployeeRepository employeeRepository;

    @AfterEach
    void cleanUp() {
        carrierRepository.deleteAll();
    }

    @Test
    void getAllActive() {
        //given
        Carrier activeCarrier = Fixture.prepareCarrier("Active Carrier");
        Carrier disabledCarrier = Carrier.builder()
                .companyName("Test name")
                .accountNumber("1234123412341234")
                .taxNumber("123412341234")
                .address("TestAdress")
                .userStatus(UserStatus.DISABLED)
                .build();
        carrierRepository.saveAll(List.of(activeCarrier, disabledCarrier));

        //when
        List<Carrier> result = carrierService.getAllActive();

        //then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result).containsExactly(activeCarrier);
    }

    @Test
    void shouldSave() {
        //given
        CarrierDto carrierDtoToSave = CarrierDto.builder()
                .companyName("To save name")
                .accountNumber("123113531123")
                .address("TestADress")
                .taxNumber("111111111111")
                .build();

        //when
        Carrier savedCarrier = carrierService.save(carrierDtoToSave);

        //then
        assertThat(carrierRepository.findAll()).contains(savedCarrier);
        assertThat(savedCarrier.getCompanyName()).isEqualTo(carrierDtoToSave.getCompanyName());
        assertThat(savedCarrier.getAddress()).isEqualTo(carrierDtoToSave.getAddress());
        assertThat(savedCarrier.getAccountNumber()).isEqualTo(carrierDtoToSave.getAccountNumber());
        assertThat(savedCarrier.getTaxNumber()).isEqualTo(carrierDtoToSave.getTaxNumber());
    }

    @Test
    void shouldUpdate() {
        //given
        Carrier originalCarrier = Fixture.prepareCarrier("Original name");
        carrierRepository.save(originalCarrier);

        CarrierDto carrierDetailsToUpdate = CarrierDto.builder()
                .id(originalCarrier.getId())
                .companyName("To save name")
                .accountNumber("123113531123")
                .address("TestADress")
                .taxNumber("111111111111")
                .build();

        //when
        carrierService.update(carrierDetailsToUpdate);

        //then
        Carrier updatedCarrier = carrierRepository.findById(originalCarrier.getId()).orElseThrow();

        assertThat(updatedCarrier.getCompanyName()).isEqualTo(carrierDetailsToUpdate.getCompanyName());
        assertThat(updatedCarrier.getAddress()).isEqualTo(carrierDetailsToUpdate.getAddress());
        assertThat(updatedCarrier.getTaxNumber()).isEqualTo(carrierDetailsToUpdate.getTaxNumber());
        assertThat(updatedCarrier.getAccountNumber()).isEqualTo(carrierDetailsToUpdate.getAccountNumber());
    }

    @Test
    void shouldDeleteCarrier() {
        //given
        Carrier carrierToDelete = Fixture.prepareCarrier("Carrier to delete");
        carrierRepository.save(carrierToDelete);

        //when
        carrierService.delete(carrierToDelete.getId());

        //then
        List<Carrier> allCarriersAfterDeletion = carrierRepository.findAll();
        assertThat(allCarriersAfterDeletion).isEmpty();
    }

    @Test
    @Transactional
    void shouldAnonymizeCarrier() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrierBeforeAnonymization = carrierRepository.save(Fixture.prepareCarrier("Carrier Test to anonymize"));

        transportOrderRepository.save(Fixture.prepareOrder(carrierBeforeAnonymization, client, employee));

        //when
        carrierService.delete(carrierBeforeAnonymization.getId());

        //then
        Carrier carrierAfterAnonymization = carrierRepository.findById(carrierBeforeAnonymization.getId()).get();

        assertThat(carrierAfterAnonymization.getCompanyName()).isEqualTo(carrierBeforeAnonymization.getCompanyName().substring(0, 1));
        assertThat(carrierAfterAnonymization.getAddress()).isEqualTo(carrierBeforeAnonymization.getAddress().substring(0, 1));
        assertThat(carrierAfterAnonymization.getTaxNumber()).isEqualTo(carrierBeforeAnonymization.getTaxNumber().substring(0, 1));
        assertThat(carrierAfterAnonymization.getAccountNumber()).isEqualTo(carrierBeforeAnonymization.getAccountNumber().substring(0, 1));
    }
}
