package com.lukash.logistics.feature.user.client.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.transport.repository.CargoRepository;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.dto.ClientDto;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ClientServiceTest extends IntegrationTest {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    ClientService clientService;
    @Autowired
    CargoRepository cargoRepository;
    @Autowired
    TransportOrderRepository transportOrderRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    CarrierRepository carrierRepository;

    @AfterEach
    void cleanUp() {
        clientRepository.deleteAll();
    }

    @Test
    void shouldGetAllActiveClients() {
        //given
        List<Client> activeClients = Fixture.prepareClients(5, UserStatus.ACTIVE);
        List<Client> disabledClients = Fixture.prepareClients(5, UserStatus.DISABLED);
        clientRepository.saveAll(activeClients);
        clientRepository.saveAll(disabledClients);

        //when
        List<Client> result = clientService.getAllActiveClients();

        //then
        assertThat(result.size()).isEqualTo(activeClients.size());
        assertThat(result).containsExactlyInAnyOrderElementsOf(activeClients);
    }

    @Test
    void shouldSave() {
        //given
        ClientDto clientToSave = ClientDto.builder()
                .companyName("Test")
                .accountNumber("12341231234")
                .address("TestADress")
                .userStatus(UserStatus.ACTIVE)
                .taxNumber("23")
                .build();

        //when
        Client savedClient = clientService.save(clientToSave);

        //then
        assertThat(clientRepository.findAll()).contains(savedClient);
    }

    @Test
    void shouldUpdate() {
        //given
        Client originalClient = Fixture.prepareClient("Client to update", UserStatus.ACTIVE);
        clientRepository.save(originalClient);

        ClientDto updateDetailsToUpdate = ClientDto.builder()
                .id(originalClient.getId())
                .companyName("updated name")
                .address("Updated")
                .accountNumber("111111111111 update")
                .taxNumber("7")
                .build();

        //when
        clientService.update(updateDetailsToUpdate);

        //then
        Client updatedClient = clientRepository.findById(originalClient.getId()).orElseThrow();

        assertThat(updatedClient.getCompanyName()).isEqualTo(updateDetailsToUpdate.getCompanyName());
        assertThat(updatedClient.getAddress()).isEqualTo(updateDetailsToUpdate.getAddress());
        assertThat(updatedClient.getAccountNumber()).isEqualTo(updateDetailsToUpdate.getAccountNumber());
        assertThat(updatedClient.getTaxNumber()).isEqualTo(updateDetailsToUpdate.getTaxNumber());
    }

    @Test
    void shouldDeleteClient() {
        //given
        Client clientToDelete = Fixture.prepareClient("Client to delete", UserStatus.ACTIVE);
        clientRepository.save(clientToDelete);

        //when
        clientService.delete(clientToDelete.getId());

        //then
        assertThat(clientRepository.findAll()).isEmpty();
    }

    @Test
    @Transactional
    void shouldAnonymizeClient() {
        //given
        Client clientBeforeAnonymization = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrierBeforeAnonymization = carrierRepository.save(Fixture.prepareCarrier("Carrier Test to anonymize"));

        transportOrderRepository.save(Fixture.prepareOrder(carrierBeforeAnonymization, clientBeforeAnonymization, employee));

        //when
        clientService.delete(clientBeforeAnonymization.getId());

        //then
        Client clientAfterAnonymizing = clientRepository.findById(clientBeforeAnonymization.getId()).get();

        assertThat(clientAfterAnonymizing.getCompanyName()).isEqualTo(clientAfterAnonymizing.getCompanyName().substring(0, 1));
        assertThat(clientAfterAnonymizing.getAddress()).isEqualTo(clientAfterAnonymizing.getAddress().substring(0, 1));
        assertThat(clientAfterAnonymizing.getTaxNumber()).isEqualTo(clientAfterAnonymizing.getTaxNumber().substring(0, 1));
        assertThat(clientAfterAnonymizing.getAccountNumber()).isEqualTo(clientAfterAnonymizing.getAccountNumber().substring(0, 1));
        assertThat(clientAfterAnonymizing.getUserStatus()).isEqualTo(UserStatus.DISABLED);
    }
}
