package com.lukash.logistics.feature.user.employee.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class EmployeeTest {

    @ParameterizedTest
    @MethodSource("prepareSource")
    void shouldTestIsAccountActive(LocalDate startDate, LocalDate endDate, boolean expectedResult) {
        //given
        final Employee employee = Employee.builder()
                .dateStarted(startDate)
                .dateEnded(endDate)
                .build();
        employee.setDateStarted(startDate);
        employee.setDateEnded(endDate);

        //when
        boolean result = employee.isAccountActive();

        //then
        assertThat(result).isEqualTo(expectedResult);
    }

    public static Stream<Arguments> prepareSource() {
        var today = LocalDate.now();
        return Stream.of(
                Arguments.of(today.minusDays(12), today.plusDays(12), true),
                Arguments.of(today.plusDays(12), today.plusDays(24), false),
                Arguments.of(today.minusDays(50), today.minusDays(25), false),
                Arguments.of(today.minusDays(1), null, true)
        );
    }
}
