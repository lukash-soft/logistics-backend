package com.lukash.logistics.feature.user.employee.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.user.employee.dto.PasswordChangeDto;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class UserPasswordServiceTest extends IntegrationTest {

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    UserPasswordService userPasswordService;

    @AfterEach
    void cleanUp() {
        employeeRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = Fixture.EMPLOYEE_EMAIL)
    void shouldUpdatePassword() {
        //given
        Employee originalUser = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE, Fixture.EMPLOYEE_EMAIL));

        PasswordChangeDto passwordChangeDto = PasswordChangeDto.builder()
                .oldPassword(originalUser.getPassword())
                .newPassword("new pass")
                .build();

        //when
        userPasswordService.updatePassword(passwordChangeDto);

        //then
        Employee updatedOriginalUser = employeeRepository.findById(originalUser.getId()).orElseThrow();
        assertThat(originalUser.getPassword()).isNotEqualTo(updatedOriginalUser.getPassword());
    }

    @Test
    void shouldResetPassword() {
        //given
        Employee originalUser = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        //when
        userPasswordService.resetPassword(originalUser.getEmail());

        //then
        Employee updatedOriginalUser = employeeRepository.findById(originalUser.getId()).orElseThrow();
        assertThat(updatedOriginalUser.getPassword()).isEqualTo("welcome");
    }

    @Test
    @WithMockUser(username = Fixture.EMPLOYEE_EMAIL)
    void shouldUpdateWithEncodePassword() {
        //given
        final Employee originalUser = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE, Fixture.EMPLOYEE_EMAIL));

        final PasswordChangeDto passwordChangeDto = PasswordChangeDto.builder()
                .newPassword("someNewPassword")
                .build();

        //when
        userPasswordService.updatePassword(passwordChangeDto);

        //then
        final List<Employee> allEmployees = employeeRepository.findAll();
        assertThat(allEmployees).hasSize(1);

        final Employee saved = allEmployees.get(0);
        assertThat(saved.getPassword())
                .isNotEqualTo(originalUser.getPassword())
                .isNotEqualTo(passwordChangeDto.getNewPassword());
    }
}
