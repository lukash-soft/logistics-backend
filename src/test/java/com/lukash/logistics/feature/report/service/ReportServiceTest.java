package com.lukash.logistics.feature.report.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.report.model.Report;
import com.lukash.logistics.feature.report.repository.ReportRepository;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.repository.CargoRepository;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ReportServiceTest extends IntegrationTest {

    @Autowired
    private ReportService reportService;
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private TransportOrderRepository transportOrderRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private CargoRepository cargoRepository;
    @Autowired
    private CarrierRepository carrierRepository;
    @Autowired
    private EmployeeRepository employeeRepository;


    @AfterEach
    void cleanUp() {
        reportRepository.deleteAll();
    }

    @Test
    @Transactional
    void generateReport() {
        //given
        LocalDate reportDate = LocalDate.now().withDayOfMonth(1).minusMonths(1);

        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));

        List<TransportOrder> completedOrdersInTime = Fixture.prepareCompleteOrdersInTime(client, employee, carrier);
        transportOrderRepository.saveAll(completedOrdersInTime);
        List<TransportOrder> completedOrdersInTimeDelayed = Fixture.prepareCompleteOrdersDelayed(client, employee, carrier);
        transportOrderRepository.saveAll(completedOrdersInTimeDelayed);
        List<TransportOrder> inProgressOrdersDelayed = Fixture.prepareOrdersInProgressDelayed(client, employee, carrier);
        transportOrderRepository.saveAll(inProgressOrdersDelayed);
        List<TransportOrder> inProgressOrders = Fixture.prepareOrdersInProgress(client, employee, carrier);
        transportOrderRepository.saveAll(inProgressOrders);
        List<TransportOrder> cancelledOrders = Fixture.prepareCancelledOrders(client, employee, carrier);
        transportOrderRepository.saveAll(cancelledOrders);

        double totalPriceOfCompletedOrders = completedOrdersInTime.stream().mapToDouble(TransportOrder::getTotalPrice).sum();
        double employeeCost = employee.getSalary();
        double totalPriceOfCompletedDelayedOrders = completedOrdersInTimeDelayed.stream().mapToDouble(TransportOrder::getTotalPrice).sum();
        double incomeGross = totalPriceOfCompletedDelayedOrders + totalPriceOfCompletedOrders;
        double fees = completedOrdersInTime.stream().mapToDouble(TransportOrder::getCarrierFee).sum() +
                completedOrdersInTimeDelayed.stream().mapToDouble(TransportOrder::getCarrierFee).sum();
        double incomeNet = incomeGross - fees - employeeCost;

        //when
        Report reportGenerated = reportService.generateReport(reportDate);
        reportRepository.save(reportGenerated);

        //then
        assertThat(reportGenerated.getNumberOfOrdersCompletedInTime()).isEqualTo(completedOrdersInTime.size());
        assertThat(reportGenerated.getNumbersOfOrdersCompletedLate()).isEqualTo(completedOrdersInTimeDelayed.size());
        assertThat(reportGenerated.getNumberOfOrdersInProgressDelayed()).isEqualTo(inProgressOrdersDelayed.size());
        assertThat(reportGenerated.getNumbersOfOrdersInProgress()).isEqualTo(inProgressOrders.size() + inProgressOrdersDelayed.size());
        assertThat(reportGenerated.getNumbersOfOrdersCanceled()).isEqualTo(cancelledOrders.size());

        assertThat(reportGenerated.getEmployeeCost()).isEqualTo(employeeCost);
        assertThat(reportGenerated.getFees()).isEqualTo(fees);
        assertThat(reportGenerated.getIncomeGross()).isEqualTo(incomeGross);
        assertThat(reportGenerated.getIncomeNet()).isEqualTo(incomeNet);
    }
}
