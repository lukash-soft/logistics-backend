package com.lukash.logistics.feature.invoice;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.company.repository.CompanyRepository;
import com.lukash.logistics.feature.invoice.exception.DuplicatedInvoiceException;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import com.lukash.logistics.feature.transport.repository.OrderHistoryRepository;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.transport.service.TransportOrderService;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class InvoiceServiceTest extends IntegrationTest {
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    CarrierRepository carrierRepository;
    @Autowired
    TransportOrderRepository transportOrderRepository;
    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    TransportOrderService transportOrderService;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    OrderHistoryRepository orderHistoryRepository;

    @AfterEach
    void cleanUp() {
        invoiceRepository.deleteAll();
        orderHistoryRepository.deleteAll();
        transportOrderRepository.deleteAll();
        carrierRepository.deleteAll();
        employeeRepository.deleteAll();
    }

    @Test
    @WithMockUser(Fixture.EMPLOYEE_EMAIL)
    void shouldGenerateInvoiceWhenStatusIsUpdated() {
        //given
        companyRepository.save(Fixture.prepareCompany("Root company"));
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE, Fixture.EMPLOYEE_EMAIL));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        TransportOrder transportOrder = Fixture.prepareOrder(carrier, client, employee);
        UserDetails details = Fixture.prepareUser(employee);
        transportOrderRepository.save(transportOrder);

        Client secondClient = clientRepository.save(Fixture.prepareClient("Second client", UserStatus.ACTIVE));
        Employee secondEmployee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier secondCarrier = carrierRepository.save(Fixture.prepareCarrier("Carrier second Test company"));
        TransportOrder secondOder = Fixture.prepareOrder(secondCarrier, secondClient, secondEmployee);
        transportOrderRepository.save(secondOder);

        //when
        transportOrderService.updateStatus(transportOrder.getId(), TransportStatus.COMPLETE, details);

        //then
        List<Invoice> invoices = invoiceRepository.findAll();
        assertThat(invoices).hasSize(1);
    }

    @Test
    @WithMockUser(Fixture.EMPLOYEE_EMAIL)
    void shouldGenerateInvoiceManually() {
        //given
        companyRepository.save(Fixture.prepareCompany("Root company"));
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE, Fixture.EMPLOYEE_EMAIL));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        TransportOrder transportOrder = Fixture.prepareOrder(carrier, client, employee);
        transportOrder.setDateEndedReal(LocalDate.now());
        transportOrder.setStatus(TransportStatus.COMPLETE);
        transportOrderRepository.save(transportOrder);

        Client secondClient = clientRepository.save(Fixture.prepareClient("Second client", UserStatus.ACTIVE));
        Employee secondEmployee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier secondCarrier = carrierRepository.save(Fixture.prepareCarrier("Carrier second Test company"));
        TransportOrder secondOder = Fixture.prepareOrder(secondCarrier, secondClient, secondEmployee);
        transportOrderRepository.save(secondOder);

        //when
        invoiceService.generateInvoice(transportOrder.getId());

        //then
        List<Invoice> invoices = invoiceRepository.findAll();
        assertThat(invoices).hasSize(1);
    }

    @Test
    @WithMockUser(Fixture.EMPLOYEE_EMAIL)
    void shouldThrowExceptionWhenInvoiceAlreadyExists() {
        //given
        companyRepository.save(Fixture.prepareCompany("Root company"));
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE, Fixture.EMPLOYEE_EMAIL));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        TransportOrder transportOrder = Fixture.prepareOrder(carrier, client, employee);
        transportOrder.setDateEndedReal(LocalDate.now());
        transportOrder.setStatus(TransportStatus.COMPLETE);
        transportOrderRepository.save(transportOrder);

        Invoice invoice = Invoice.builder()
                .paymentDeadline(LocalDate.now())
                .accountNumber("0000 0000 0000 0000 0000")
                .clientName("Client test")
                .totalPrice(2233.0)
                .isPayed(false)
                .transportOrder(transportOrder)
                .build();
        invoiceRepository.save(invoice);

        //when, then
        assertThatThrownBy(() -> invoiceService.generateInvoice(transportOrder.getId())).isInstanceOf(DuplicatedInvoiceException.class);
    }
}
