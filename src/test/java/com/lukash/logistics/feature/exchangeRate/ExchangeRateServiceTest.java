package com.lukash.logistics.feature.exchangeRate;

import com.lukash.logistics.feature.exchangeRate.model.CurrencyType;
import com.lukash.logistics.feature.exchangeRate.service.ExchangeRateService;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class ExchangeRateServiceTest extends IntegrationTest {

    @Autowired
    private ExchangeRateService exchangeRateService;


    @Test
    void shouldCountPlnPrice() {
        //given
        CurrencyType currencyCode = CurrencyType.USD;
        LocalDate exchangeRateDate = LocalDate.of(2023, 5, 4);
        double askPrice = 4.2173;
        double sourceValue = 123.44;

        //when
        double result = exchangeRateService.convertToPln(sourceValue, currencyCode, exchangeRateDate);

        //then
        assertThat(result).isEqualTo(askPrice * sourceValue);
    }


}
