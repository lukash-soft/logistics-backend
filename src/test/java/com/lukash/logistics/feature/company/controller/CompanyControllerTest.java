package com.lukash.logistics.feature.company.controller;

import com.lukash.logistics.feature.company.dto.CompanyDto;
import com.lukash.logistics.feature.company.model.Company;
import com.lukash.logistics.feature.company.repository.CompanyRepository;
import com.lukash.logistics.setup.DatabaseFixture;
import com.lukash.logistics.setup.IntegrationTest;
import com.lukash.logistics.setup.MockingEmployeeOne;
import com.lukash.logistics.setup.MockingOwner;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.Matchers.greaterThan;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CompanyControllerTest extends IntegrationTest {

    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    DatabaseFixture fixture;

    @BeforeEach
    void prepareData() {
        fixture.prepareCompany();
    }

    @AfterEach
    void cleanup() {
        fixture.cleanup();
    }

    @Test
    @MockingOwner
    @SneakyThrows
    void shouldSaveIfOwner() {
        //given
        companyRepository.deleteAll();
        final CompanyDto dto = createDto();

        //when, then
        postRequest("/companies", dto)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", greaterThan(0)));
    }

    @Test
    @MockingEmployeeOne
    @SneakyThrows
    void shouldNotSaveIfEmployee() {
        //given
        final CompanyDto dto = createDto();

        //when, then
        assertThatThrownBy(() -> postRequest("/companies", dto)).isInstanceOf(Exception.class);
    }

    @Test
    @SneakyThrows
    @MockingOwner
    void shouldUpdateIfOwner() {
        //given
        final Company existing = companyRepository.findAll().get(0);

        final CompanyDto dto = CompanyDto.builder()
                .id(existing.getId())
                .companyName("NEW NAME")
                .accountNumber(existing.getAccountNumber())
                .address(existing.getAddress())
                .taxNumber(existing.getTaxNumber())
                .build();

        putRequest("/companies", dto)
                .andExpect(status().isOk());
    }


    private static CompanyDto createDto() {
        return CompanyDto.builder()
                .companyName("name")
                .accountNumber("124566")
                .address("adddd")
                .taxNumber("asdasd")
                .build();
    }
}
