package com.lukash.logistics.feature.company.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.company.dto.CompanyDto;
import com.lukash.logistics.feature.company.model.Company;
import com.lukash.logistics.feature.company.repository.CompanyRepository;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class CompanyServiceTest extends IntegrationTest {

    @Autowired
    CompanyService companyService;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    EmployeeRepository employeeRepository;

    @AfterEach
    void cleanUp() {
        employeeRepository.deleteAll();
        companyRepository.deleteAll();
    }

    @Test
    void shouldGetCompany() {
        //given
        Company companyToGet = Fixture.prepareCompany("Test company to get");
        companyRepository.save(companyToGet);

        //when
        Optional<CompanyDto> companyDtoOptional = companyService.get();

        //then
        assertThat(companyDtoOptional).isPresent();
    }

    @Test
    void shouldSave() {
        //given
        Company companyToSave = Fixture.prepareCompany("Test Company to save");
        Employee employee = Fixture.prepareEmployee(UserRole.OWNER);
        employeeRepository.save(employee);

        //when
        Company savedCompany = companyService.save(CompanyDto.of(companyToSave));

        //then
        assertThat(companyRepository.findAll()).contains(savedCompany);
    }

    @Test
    void shouldUpdate() {
        //given
        Company originalCompany = companyRepository.save(Fixture.prepareCompany("Company to update"));

        CompanyDto companyUpdateDetails = CompanyDto.builder()
                .id(originalCompany.getId())
                .companyName("Updated name")
                .address("updated adress")
                .accountNumber("11111111111111")
                .taxNumber("543213678")
                .build();

        //when
        companyService.update(companyUpdateDetails);

        //then
        Company updatedCompany = companyRepository.findById(originalCompany.getId()).orElseThrow();

        assertThat(updatedCompany.getCompanyName()).isEqualTo(companyUpdateDetails.getCompanyName());
        assertThat(updatedCompany.getAddress()).isEqualTo(companyUpdateDetails.getAddress());
        assertThat(updatedCompany.getAccountNumber()).isEqualTo(companyUpdateDetails.getAccountNumber());
        assertThat(updatedCompany.getTaxNumber()).isEqualTo(companyUpdateDetails.getTaxNumber());
    }

    @Test
    void shouldTestUniqueNumberAccountAndTaxNumber() {
        //given
        Company companyOne = companyRepository.save(Fixture.prepareCompany("first company"));
        Company companyTwo = companyRepository.save(Fixture.prepareCompany("Company Two"));

        //when,then
        assertThat(companyOne.getAccountNumber()).isNotEqualTo(companyTwo.getAccountNumber());
        assertThat(companyOne.getTaxNumber()).isNotEqualTo(companyTwo.getTaxNumber());

    }
}
