package com.lukash.logistics.feature.transport.damageReport.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.damageReport.dto.DamageReportDto;
import com.lukash.logistics.feature.damageReport.dto.DamageReportUpdateDto;
import com.lukash.logistics.feature.damageReport.model.DamageReport;
import com.lukash.logistics.feature.damageReport.repository.DamageReportRepository;
import com.lukash.logistics.feature.damageReport.service.DamageReportService;
import com.lukash.logistics.feature.files.model.TransportOrderFile;
import com.lukash.logistics.feature.files.repository.TransportOrderFileRepository;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.repository.CargoRepository;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Transactional
class DamageReportServiceTest extends IntegrationTest {

    @Autowired
    DamageReportRepository damageReportRepository;
    @Autowired
    DamageReportService damageReportService;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    TransportOrderRepository transportOrderRepository;
    @Autowired
    CarrierRepository carrierRepository;
    @Autowired
    CargoRepository cargoRepository;
    @Autowired
    TransportOrderFileRepository transportOrderFileRepository;
    @Autowired
    EmployeeRepository employeeRepository;

    @Value("${files.orders}")
    String rootDirectoryPath;

    @AfterEach
    void cleanUp() throws IOException {
        damageReportRepository.deleteAll();
        FileUtils.deleteDirectory(new File(rootDirectoryPath));
    }


    @Test
    void shouldGetReportByOrderId() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));

        TransportOrder transportOrder = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, employee));
        transportOrderRepository.save(transportOrder);

        List<TransportOrderFile> orderFiles =
                Fixture.prepareOrderFiles(transportOrder.getSupervisor(), 3, transportOrder);
        transportOrderFileRepository.saveAll(orderFiles);

        DamageReport damageReportToGet = Fixture.prepareDamageReport(transportOrder, orderFiles);
        damageReportRepository.save(damageReportToGet);

        //when
        DamageReport result = damageReportService.getReportByOrderId(transportOrder.getId());

        //then
        assertThat(result.getId()).isEqualTo(damageReportToGet.getId());
    }

    @Test
    void shouldSave() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(employee);
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));

        TransportOrder transportOrder = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, employee));
        transportOrderRepository.save(transportOrder);

        final var file = new File("src/test/resources/files/damageFile.pdf");
        final MultipartFile multipartFile = Fixture.createMultipartFile(file);

        DamageReportDto damageReportDto = DamageReportDto.builder()
                .reportedById(employee.getId())
                .description("Test Descr of damage file")
                .build();

        //when
        damageReportService.save(transportOrder.getId(), damageReportDto, multipartFile, details);

        //then
        List<DamageReport> damageReports = damageReportRepository.findAll();
        assertThat(damageReports).isNotEmpty();
    }

    @Test
    void shouldUpdate() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        UserDetails details = Fixture.prepareUser(employee);

        TransportOrder transportOrder = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, employee));
        transportOrderRepository.save(transportOrder);

        List<TransportOrderFile> orderFiles = Fixture.prepareOrderFiles(employee, 3, transportOrder);
        transportOrderFileRepository.saveAll(orderFiles);

        DamageReport damageReportToUpdate = DamageReport.builder()
                .supervisor(employee)
                .orderFile(orderFiles)
                .transportOrder(transportOrder)
                .description("Test descr")
                .build();
        damageReportRepository.save(damageReportToUpdate);

        DamageReportUpdateDto damageReportUpdateDto = DamageReportUpdateDto.builder()
                .description("Updated dsecr")
                .build();

        //when
        damageReportService.update(transportOrder.getId(), damageReportUpdateDto, details);
        //then
        DamageReport reportUpdated = damageReportRepository.findById(damageReportToUpdate.getId()).get();
        assertThat(reportUpdated.getDescription()).isEqualTo(damageReportUpdateDto.getDescription());
    }

    @Test
    void shouldDelete() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(employee);
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));

        TransportOrder transportOrder = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, employee));
        transportOrderRepository.save(transportOrder);

        List<TransportOrderFile> orderFiles = Fixture.prepareOrderFiles(employee, 3, transportOrder);
        transportOrderFileRepository.saveAll(orderFiles);

        DamageReport damageReportToDelete = DamageReport.builder()
                .supervisor(employee)
                .orderFile(orderFiles)
                .transportOrder(transportOrder)
                .description("Test descr")
                .build();
        damageReportRepository.save(damageReportToDelete);
        List<DamageReport> damageReportsBeforeDeletion = damageReportRepository.findAll();

        //when
        damageReportService.delete(transportOrder.getId(), details);

        //then
        List<DamageReport> damageReportsAfterDeletion = damageReportRepository.findAll();
        assertThat(damageReportsAfterDeletion).hasSize(damageReportsBeforeDeletion.size() - 1);
    }
}
