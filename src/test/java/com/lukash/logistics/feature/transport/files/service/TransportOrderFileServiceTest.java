package com.lukash.logistics.feature.transport.files.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.files.dto.AddFileDto;
import com.lukash.logistics.feature.files.model.TransportOrderFile;
import com.lukash.logistics.feature.files.repository.TransportOrderFileRepository;
import com.lukash.logistics.feature.files.service.TransportOrderFileService;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.repository.CargoRepository;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class TransportOrderFileServiceTest extends IntegrationTest {
    @Autowired
    TransportOrderFileService transportOrderFileService;
    @Autowired
    TransportOrderRepository transportOrderRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    CargoRepository cargoRepository;
    @Autowired
    CarrierRepository carrierRepository;
    @Autowired
    TransportOrderFileRepository transportOrderFileRepository;

    @Value("${files.orders}")
    String rootDirectoryPath;

    @AfterEach
    void cleanUp() throws IOException {
        transportOrderRepository.deleteAll();
        FileUtils.deleteDirectory(new File(rootDirectoryPath));
    }

    @Test
    @Transactional
    void shouldGetSummary() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));

        TransportOrder transportOrder = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, employee));
        List<TransportOrderFile> orderFileList = Fixture.prepareOrderFiles(employee, 5, transportOrder);
        transportOrderFileRepository.saveAll(orderFileList);
        //when
        List<TransportOrderFile> result = transportOrderFileService.getSummary(transportOrder.getId());

        //then
        assertThat(result).hasSize(5);
    }

    @Test
    @Transactional
    void getById() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));

        TransportOrder transportOrder = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, employee));
        TransportOrderFile orderFile = transportOrderFileRepository.save(Fixture.prepareOrderFile(employee, transportOrder));

        //when
        byte[] result = transportOrderFileService.getById(orderFile.getId());

        //then
        assertThat(result).isNotEmpty();
    }

    @Test
    @Transactional
    void shouldAddFile() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        UserDetails details = Fixture.prepareUser(employee);
        TransportOrder transportOrder = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, employee));


        final var file = new File("src/test/resources/files/orderFile.pdf");
        final MultipartFile multipartFile = Fixture.createMultipartFile(file);

        AddFileDto addFileDto = AddFileDto.builder()
                .orderId(transportOrder.getId())
                .file(multipartFile)
                .description("Test Descr")
                .build();

        //when
        transportOrderFileService.addFile(addFileDto, details);

        //then
        List<TransportOrderFile> orderFileListAfterAdd =
                transportOrderFileRepository.findAllByTransportOrderId(transportOrder.getId());
        TransportOrderFile transportOrderToFind = orderFileListAfterAdd.get(0);
        assertThat(new File(transportOrderToFind.getPath()).isFile()).isEqualTo(true);
    }



    @Test
    @Transactional
    void delete() throws IOException {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        UserDetails details = Fixture.prepareUser(employee);
        TransportOrder transportOrder = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, employee));

        Path sourceFile = Paths.get(Fixture.FILES_DIR + "/orderFile.pdf");
        Path targetPath = Paths.get(Fixture.FILES_DIR + "/copy.pdf");
        Files.copy(sourceFile, targetPath, StandardCopyOption.COPY_ATTRIBUTES);

        TransportOrderFile transportOrderFile = TransportOrderFile.builder()
                .name("Test")
                .path(targetPath.toString())
                .transportOrder(transportOrder)
                .addedBy(employee)
                .description("Test descr")
                .build();
        transportOrderFileRepository.save(transportOrderFile);

        List<TransportOrderFile> orderFilesBeforeRemoval = transportOrderFileRepository.findAllByTransportOrderId(transportOrder.getId());

        //when
        transportOrderFileService.delete(transportOrderFile.getId(), details);

        //then
        List<TransportOrderFile> orderFilesAfterRemoval = transportOrderFileRepository.findAllByTransportOrderId(transportOrder.getId());
        assertThat(orderFilesAfterRemoval.size()).isEqualTo(orderFilesBeforeRemoval.size() - 1);
    }
}
