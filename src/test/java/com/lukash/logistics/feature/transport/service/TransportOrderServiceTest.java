package com.lukash.logistics.feature.transport.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.company.repository.CompanyRepository;
import com.lukash.logistics.feature.transport.dto.CargoDto;
import com.lukash.logistics.feature.transport.dto.OrderCompleteDto;
import com.lukash.logistics.feature.transport.dto.OrderSearchCriteria;
import com.lukash.logistics.feature.transport.dto.StatusUpdateDto;
import com.lukash.logistics.feature.transport.dto.TransportOrderCsvDTO;
import com.lukash.logistics.feature.transport.dto.TransportOrderDto;
import com.lukash.logistics.feature.transport.dto.TransportRegistrationDto;
import com.lukash.logistics.feature.transport.exception.OrderAuthorizationException;
import com.lukash.logistics.feature.transport.exception.OrderStatusNotAppropriate;
import com.lukash.logistics.feature.transport.model.Cargo;
import com.lukash.logistics.feature.transport.model.OrderHistory;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.model.enums.CurrencyModel;
import com.lukash.logistics.feature.transport.model.enums.QuantityType;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import com.lukash.logistics.feature.transport.repository.CargoRepository;
import com.lukash.logistics.feature.transport.repository.OrderHistoryRepository;
import com.lukash.logistics.feature.transport.repository.TransportOrderRepository;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.feature.user.employee.exception.SupervisorAlreadyAssigned;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Transactional
class TransportOrderServiceTest extends IntegrationTest {

    @Autowired
    TransportOrderRepository transportOrderRepository;
    @Autowired
    TransportOrderService transportOrderService;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    CargoRepository cargoRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    CarrierRepository carrierRepository;
    @Autowired
    OrderHistoryRepository orderHistoryRepository;
    @Autowired
    CompanyRepository companyRepository;


    @AfterEach
    void cleanUp() {
        transportOrderRepository.deleteAll();
        employeeRepository.deleteAll();
        carrierRepository.deleteAll();
        clientRepository.deleteAll();
    }

    @Test
    void shouldGetAll() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));

        TransportOrder transportOrderToGet = Fixture.prepareOrder(carrier, client, employee);

        transportOrderRepository.save(transportOrderToGet);

        //when
        List<TransportOrder> result = transportOrderService.getAll();

        //then
        assertThat(result).containsExactly(transportOrderToGet);
    }

    @Test
    void shouldGetByDatesAndClient() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        List<TransportOrder> orders = IntStream.rangeClosed(0, 5).mapToObj(order -> Fixture.prepareOrder(carrier, client, employee)).toList();
        transportOrderRepository.saveAll(orders);

        OrderSearchCriteria orderSearchCriteria = OrderSearchCriteria.builder()
                .client(client.getId())
                .plannedStartFrom((LocalDate.of(2023, 1, 1)))
                .plannedStartTo(LocalDate.now().plusDays(31))
                .build();

        //when
        List<TransportOrder> result = transportOrderService.getAllByDatesAndClient(orderSearchCriteria);

        //Then
        assertThat(result).isNotEmpty();
        assertThat(result).containsAll(orders);
    }

    @Test
    @WithMockUser(Fixture.EMPLOYEE_EMAIL)
    void shouldThrowExceptionWhenSupervisorIsNotAppropriateWhenCreatingInvoice() {
        //given
        Employee loggedEmployee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE, Fixture.EMPLOYEE_EMAIL));

        companyRepository.save(Fixture.prepareCompany("Root company"));
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Employee supervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE, "Otheremail@gmail.com"));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        TransportOrder transportOrder = Fixture.prepareOrder(carrier, client, supervisor);
        UserDetails userDetails = Fixture.prepareUser(loggedEmployee);
        transportOrder.setDateEndedReal(LocalDate.now());
        transportOrder.setStatus(TransportStatus.COMPLETE);
        transportOrderRepository.save(transportOrder);

        //when, then
        assertThatThrownBy(() -> transportOrderService.updateStatus(transportOrder.getId(), TransportStatus.COMPLETE, userDetails)).isInstanceOf(OrderAuthorizationException.class);
    }

    @Test
    void shouldGetByDatesAndClientWhenPlannedStartFromIsNull() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        List<TransportOrder> orders = IntStream.rangeClosed(0, 5)
                .mapToObj(order -> Fixture.prepareOrder(carrier, client, employee))
                .toList();
        transportOrderRepository.saveAll(orders);

        OrderSearchCriteria orderSearchCriteria = OrderSearchCriteria.builder()
                .client(client.getId())
                .plannedStartTo(LocalDate.now().plusDays(31))
                .build();

        //when
        List<TransportOrder> result = transportOrderService.getAllByDatesAndClient(orderSearchCriteria);

        //Then
        assertThat(orderSearchCriteria.getPlannedStartFrom()).isEqualTo(LocalDate.of(1900, 1, 1));
        assertThat(result).isNotEmpty();
        assertThat(result).containsAll(orders);
    }

    @Test
    void shouldGetByDatesAndClientWhenPlannedStartToIsNull() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        List<TransportOrder> orders = IntStream.rangeClosed(0, 5)
                .mapToObj(order -> Fixture.prepareOrder(carrier, client, employee))
                .toList();
        transportOrderRepository.saveAll(orders);

        List<TransportOrder> ordersWithEndedDatePlannedAfterDefault = IntStream.rangeClosed(0, 5)
                .mapToObj(order -> Fixture.prepareOrder(carrier, client, employee))
                .toList();
        ordersWithEndedDatePlannedAfterDefault.forEach(order -> order.setDateEndedPlanned(LocalDate.of(3001, 1, 1)));
        transportOrderRepository.saveAll(orders);

        OrderSearchCriteria orderSearchCriteria = OrderSearchCriteria.builder()
                .client(client.getId())
                .plannedStartFrom(LocalDate.of(2022, 3, 3))
                .build();

        //when
        List<TransportOrder> result = transportOrderService.getAllByDatesAndClient(orderSearchCriteria);

        //Then
        assertThat(orderSearchCriteria.getPlannedStartTo()).isEqualTo(LocalDate.of(3000, 1, 1));
        assertThat(result).isNotEmpty();
        assertThat(result.size()).isEqualTo(orders.size());
    }

    @Test
    void shouldGetByDatesAndClientWhenClientIsNull() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        List<TransportOrder> orders = IntStream.rangeClosed(0, 5).mapToObj(order -> Fixture.prepareOrder(carrier, client, employee)).toList();
        transportOrderRepository.saveAll(orders);

        OrderSearchCriteria orderSearchCriteria = OrderSearchCriteria.builder()
                .client(null)
                .plannedStartFrom((LocalDate.of(2023, 1, 1)))
                .plannedStartTo(LocalDate.now().plusDays(31))
                .build();

        //when
        List<TransportOrder> result = transportOrderService.getAllByDatesAndClient(orderSearchCriteria);

        //Then
        assertThat(result).isNotEmpty();
        assertThat(result).containsAll(orders);
    }


    @Test
    void shouldSave() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Cargo cargo = cargoRepository.save(Fixture.prepareCargo());
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(employee);

        TransportRegistrationDto transportRegistrationDto = TransportRegistrationDto.builder()
                .addressFrom("Test adres fromt")
                .addressTo("test adres to")
                .dateEndedPlanned(LocalDate.now().plusDays(60))
                .carrierFee(4000.0)
                .dateStartedPlanned(LocalDate.now())
                .status(TransportStatus.IN_PROGRESS).vehicleNumber("ddddddd")
                .totalPrice(50000.0)
                .cargoDto(CargoDto.of(cargo))
                .clientId(client.getId())
                .carrierId(carrier.getId())
                .build();

        //when
        TransportOrder savedTransportOrder = transportOrderService.save(transportRegistrationDto, details);

        //then
        OrderHistory orderHistory = orderHistoryRepository.findAll().get(0);

        assertThat(transportOrderRepository.findAll()).contains(savedTransportOrder);
        assertThat(orderHistoryRepository.findAll().size()).isEqualTo(1);
        assertThat(orderHistory.getTransportStatus()).isEqualTo(TransportStatus.CREATED);
        assertThat(savedTransportOrder.getStatus()).isEqualTo(TransportStatus.CREATED);
    }

    @ParameterizedTest
    @EnumSource(value = TransportStatus.class, names = {"IN_PROGRESS", "CANCELLED", "COMPLETE"})
    @WithMockUser(Fixture.EMPLOYEE_EMAIL)
    void shouldUpdateTransportOrderStatus(TransportStatus transportStatus) {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE, Fixture.EMPLOYEE_EMAIL));
        UserDetails details = Fixture.prepareUser(employee);

        TransportOrder transportOrderToUpdate = Fixture.prepareOrder(carrier, client, employee);
        transportOrderRepository.save(transportOrderToUpdate);

        StatusUpdateDto statusUpdateDto = StatusUpdateDto.builder()
                .id(transportOrderToUpdate.getId())
                .transportStatus(transportStatus)
                .build();

        //when
        transportOrderService.updateStatus(statusUpdateDto.getId(), statusUpdateDto.getTransportStatus(), details);

        //then
        TransportOrder updatedTransportOrder = transportOrderRepository.findById(transportOrderToUpdate.getId()).get();

        assertThat(updatedTransportOrder.getStatus()).isEqualTo(statusUpdateDto.getTransportStatus());
        assertThat(orderHistoryRepository.findAll().size()).isEqualTo(1);
    }

    @ParameterizedTest
    @MethodSource("prepareSource")
    void shouldThrowExceptionWhenUpdatingToTheInappropriateStatus(TransportStatus input, TransportStatus updated) {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(employee);

        TransportOrder transportOrderToUpdate = Fixture.prepareOrder(carrier, client, employee);
        transportOrderToUpdate.setStatus(input);
        transportOrderRepository.save(transportOrderToUpdate);

        StatusUpdateDto statusUpdateDto = StatusUpdateDto.builder()
                .id(transportOrderToUpdate.getId())
                .transportStatus(updated)
                .build();

        //when
        assertThatThrownBy(() -> transportOrderService.updateStatus(statusUpdateDto.getId(), statusUpdateDto.getTransportStatus(), details))
                .isInstanceOf(OrderStatusNotAppropriate.class);
    }

    static Stream<Arguments> prepareSource() {
        return Stream.of(
                Arguments.of(TransportStatus.CREATED, TransportStatus.PAUSED),
                Arguments.of(TransportStatus.CANCELLED, TransportStatus.PAUSED),
                Arguments.of(TransportStatus.COMPLETE, TransportStatus.PAUSED),
                Arguments.of(TransportStatus.COMPLETE, TransportStatus.CANCELLED)
        );
    }

    @Test
    void shouldUpdateStatusFromCompletedAndDeleteRealDeliveryDate() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(employee);

        TransportOrder transportOrderToUpdate = Fixture.prepareOrder(carrier, client, employee);
        transportOrderToUpdate.setStatus(TransportStatus.COMPLETE);
        transportOrderRepository.save(transportOrderToUpdate);

        StatusUpdateDto statusUpdateDto = StatusUpdateDto.builder()
                .id(transportOrderToUpdate.getId())
                .transportStatus(TransportStatus.IN_PROGRESS)
                .build();

        //when
        transportOrderService.updateStatus(statusUpdateDto.getId(), statusUpdateDto.getTransportStatus(), details);

        //then
        TransportOrder updatedTransportOrder = transportOrderRepository.findById(transportOrderToUpdate.getId()).get();

        assertThat(updatedTransportOrder.getStatus()).isEqualTo(statusUpdateDto.getTransportStatus());
        assertThat(updatedTransportOrder.getDateEndedReal()).isNull();
    }

    @Test
    void shouldThrowExceptionWhenUserIsUnauthorized() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee loggedEmployee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(loggedEmployee);
        Employee employeeToTest = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        TransportOrder transportOrderToUpdate = Fixture.prepareOrder(carrier, client, employeeToTest);
        transportOrderRepository.save(transportOrderToUpdate);

        StatusUpdateDto statusUpdateDto = StatusUpdateDto.builder().id(transportOrderToUpdate.getId()).transportStatus(TransportStatus.IN_PROGRESS).build();

        //when, then
        assertThatThrownBy(() -> transportOrderService.updateStatus(statusUpdateDto.getId(), statusUpdateDto.getTransportStatus(), details)).isInstanceOf(OrderAuthorizationException.class);
    }

    @Test
    void shouldTestUpdatingSupervisor() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee formerSupervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        Employee replacingSupervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(replacingSupervisor);

        TransportOrder transportOrderToUpdate = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, formerSupervisor));

        //when
        transportOrderService.updateSupervisor(transportOrderToUpdate.getId(), details);

        //then
        TransportOrder updatedTransportOrder = transportOrderRepository.findById(transportOrderToUpdate.getId()).get();
        assertThat(updatedTransportOrder.getSupervisor().getId()).isEqualTo(replacingSupervisor.getId());
    }

    @Test
    void shouldThrowExceptionWhenRequestingUserIsTheSameAsSupervisor() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee supervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(supervisor);

        TransportOrder transportOrderToUpdate = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, supervisor));

        //when, then
        assertThatThrownBy(() -> transportOrderService.updateSupervisor(transportOrderToUpdate.getId(), details)).isInstanceOf(SupervisorAlreadyAssigned.class);
    }

    @Test
    void shouldTestUpdatingBasicDataOfOrder() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee supervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(supervisor);

        TransportOrder transportOrderToUpdate = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, supervisor));

        CargoDto cargoDto = CargoDto.builder()
                .cargoType("test type")
                .description("This is test sugar")
                .quantity(100.2)
                .quantityType(QuantityType.PIECES)
                .build();

        TransportOrderDto transportOrderDto = TransportOrderDto.builder()
                .id(transportOrderToUpdate.getId())
                .cargoDto(cargoDto)
                .addressFrom("New Address from")
                .addressTo("New address to")
                .dateStartedPlanned(LocalDate.now().plusDays(12))
                .dateEndedPlanned(LocalDate.now().plusDays(70))
                .carrierFee(1300)
                .totalPrice(50003)
                .currency(CurrencyModel.PLN)
                .vehicleNumber("aaaaaaa")
                .build();
        //when
        transportOrderService.update(transportOrderDto, details);
        //then
        TransportOrder updatedTransportOrder = transportOrderRepository.findById(transportOrderToUpdate.getId()).get();

        assertThat(updatedTransportOrder.getAddressFrom()).isEqualTo(transportOrderDto.getAddressFrom());
        assertThat(updatedTransportOrder.getAddressTo()).isEqualTo(transportOrderDto.getAddressTo());
        assertThat(updatedTransportOrder.getCarrierFee()).isEqualTo(transportOrderDto.getCarrierFee());
        assertThat(updatedTransportOrder.getTotalPrice()).isEqualTo(transportOrderDto.getTotalPrice());
        assertThat(updatedTransportOrder.getDateStartedPlanned()).isEqualTo(transportOrderDto.getDateStartedPlanned());
        assertThat(updatedTransportOrder.getDateEndedPlanned()).isEqualTo(transportOrderDto.getDateEndedPlanned());
        assertThat(updatedTransportOrder.getVehicleNumber()).isEqualTo(transportOrderDto.getVehicleNumber());

        assertThat(updatedTransportOrder.getCargo().getCargoType()).isEqualTo(cargoDto.getCargoType());
        assertThat(updatedTransportOrder.getCargo().getQuantity()).isEqualTo(cargoDto.getQuantity());
        assertThat(updatedTransportOrder.getCargo().getQuantityType()).isEqualTo(cargoDto.getQuantityType());
        assertThat(updatedTransportOrder.getCargo().getDescription()).isEqualTo(cargoDto.getDescription());
    }

    @Test
    void shouldTestDeleteOrder() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee supervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(supervisor);

        TransportOrder orderToDelete = transportOrderRepository.save(Fixture.prepareOrder(carrier, client, supervisor));

        List<TransportOrder> ordersBeforeDeletion = transportOrderRepository.findAll();

        //when
        transportOrderService.deleteOrder(orderToDelete.getId(), details);

        //then
        List<TransportOrder> ordersAfterDeletion = transportOrderRepository.findAll();
        assertThat(ordersBeforeDeletion).contains(orderToDelete);
        assertThat(ordersAfterDeletion).isEmpty();
    }

    @Test
    void shouldThrowExceptionWhenOrderStatusIsWrong() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee supervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(supervisor);

        TransportOrder orderToDelete = Fixture.prepareOrder(carrier, client, supervisor);
        orderToDelete.setStatus(TransportStatus.IN_PROGRESS);
        transportOrderRepository.save(orderToDelete);

        //when, then
        assertThatThrownBy(() -> transportOrderService.deleteOrder(orderToDelete.getId(), details)).isInstanceOf(OrderStatusNotAppropriate.class);
    }

    @Test
    void shouldTestUpdateStatusCompleted() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee supervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(supervisor);

        TransportOrder orderToUpdate = Fixture.prepareOrder(carrier, client, supervisor);
        orderToUpdate.setDateStartedPlanned(LocalDate.now().minusDays(60));
        transportOrderRepository.save(orderToUpdate);
        OrderCompleteDto orderCompleteDto = OrderCompleteDto.builder()
                .orderId(orderToUpdate.getId())
                .build();

        //when
        transportOrderService.completeOrder(orderCompleteDto.getOrderId(), orderCompleteDto.getDeliveryDate(), details);

        //then
        TransportOrder transportOrderCompleted = transportOrderRepository.findById(orderToUpdate.getId()).get();
        assertThat(transportOrderCompleted.getStatus()).isEqualTo(TransportStatus.COMPLETE);
    }

    @Test
    void shouldThrowExceptionWhenDeliveryDateInTheFuture() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee supervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(supervisor);

        TransportOrder orderToUpdate = Fixture.prepareOrder(carrier, client, supervisor);
        orderToUpdate.setDateStartedPlanned(LocalDate.now().minusDays(40));
        transportOrderRepository.save(orderToUpdate);

        OrderCompleteDto orderCompleteDto = OrderCompleteDto.builder()
                .orderId(orderToUpdate.getId())
                .deliveryDate(LocalDate.now().plusDays(12))
                .build();

        //when, then
        assertThatThrownBy(() ->
                transportOrderService.completeOrder(orderCompleteDto.getOrderId(), orderCompleteDto.getDeliveryDate(), details))
                .isInstanceOf(DateTimeException.class);
    }

    @Test
    void shouldThrowExceptionWhenDeliveryDateIsBeforeStartDate() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Employee supervisor = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        UserDetails details = Fixture.prepareUser(supervisor);

        TransportOrder orderToUpdate = Fixture.prepareOrder(carrier, client, supervisor);
        orderToUpdate.setDateStartedPlanned(LocalDate.now().minusDays(40));
        transportOrderRepository.save(orderToUpdate);

        OrderCompleteDto orderCompleteDto = OrderCompleteDto.builder()
                .orderId(orderToUpdate.getId())
                .deliveryDate(orderToUpdate.getDateStartedPlanned().minusDays(12))
                .build();

        //when, then
        assertThatThrownBy(() ->
                transportOrderService.completeOrder(orderCompleteDto.getOrderId(), orderCompleteDto.getDeliveryDate(), details))
                .isInstanceOf(DateTimeException.class);
    }

    @Test
    void shouldTestSaveBatch() {
        //given
        Client client = clientRepository.save(Fixture.prepareClient("Test client", UserStatus.ACTIVE));
        Carrier carrier = carrierRepository.save(Fixture.prepareCarrier("Carrier Test company"));
        Pair<Client, Carrier> pair = Pair.of(client, carrier);
        List<Pair<Client, Carrier>> userPerRow = List.of(pair);

        Employee employee = Fixture.prepareEmployee(UserRole.EMPLOYEE);
        employeeRepository.save(employee);
        UserDetails userDetails = Fixture.prepareUser(employee);

        final File csv = createCsv(userPerRow);
        final MultipartFile multipartFile = Fixture.createMultipartFile(csv);
        //when
        List<TransportOrder> ordersToSave = transportOrderService.saveBatch(multipartFile, userDetails);

        //then
        assertThat(ordersToSave).hasSize(1);
        assertThat(ordersToSave.get(0).getClient().getId()).isEqualTo(client.getId());
        assertThat(ordersToSave.get(0).getCarrier().getId()).isEqualTo(carrier.getId());

        assertThat(csv.delete()).isTrue();
    }

    private File createCsv(final List<Pair<Client, Carrier>> usersPerRow) {
        List<TransportOrderCsvDTO> fileData = usersPerRow.stream()
                .map(row -> Fixture.prepareOrderCsvDto(row.getLeft(), row.getRight()))
                .toList();

        Path path = Path.of("src/test/resources/files/OrderCsv.csv");

        try (Writer writer = new FileWriter(path.toString())) {
            StatefulBeanToCsv<TransportOrderCsvDTO> sbc = new StatefulBeanToCsvBuilder<TransportOrderCsvDTO>(writer).build();
            sbc.write(fileData);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return path.toFile();
    }
}
