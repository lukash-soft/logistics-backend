package com.lukash.logistics.setup;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.company.model.Company;
import com.lukash.logistics.feature.company.repository.CompanyRepository;
import com.lukash.logistics.feature.user.carrier.repository.CarrierRepository;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.client.repository.ClientRepository;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.stream.Stream;

@Component
public class DatabaseFixture {

    @Getter
    private static int companySize;
    @Getter
    private static int activeEmployeesSize;
    @Getter
    private static int carriersSize;
    @Getter
    private static int clientsSize;

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    CarrierRepository carrierRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    CompanyRepository companyRepository;

    public static final String OWNER_1 = "owner1@user";
    public static final String EMPLOYEE_1 = "employee1@user";
    public static final String EMPLOYEE_2 = "employee2@user";
    public static final String EMPLOYEE_3 = "employee3@user";
    public static final String EMPLOYEE_4_TERMINATED = "employee4@user";

    public void cleanup() {
        companyRepository.deleteAll();
        employeeRepository.deleteAll();
        clientRepository.deleteAll();
        carrierRepository.deleteAll();
    }

    public void prepareCompany() {
        companySize = Stream.of(
                        prepareEmployee(UserRole.EMPLOYEE, EMPLOYEE_1).build(),
                        prepareEmployee(UserRole.EMPLOYEE, EMPLOYEE_2).build(),
                        prepareEmployee(UserRole.EMPLOYEE, EMPLOYEE_3).build(),
                        prepareEmployee(UserRole.EMPLOYEE, EMPLOYEE_4_TERMINATED).dateEnded(LocalDate.now().minusMonths(1)).build(),

                        prepareEmployee(UserRole.OWNER, OWNER_1).build())
                .map(employeeRepository::save)
                .toList().size();

        activeEmployeesSize = companySize - 1;
        carriersSize = carrierRepository.saveAll(Fixture.prepareCarriers(3)).size();
        clientsSize = clientRepository.saveAll(Fixture.prepareClients(3, UserStatus.ACTIVE)).size();

        companyRepository.save(getCompanyDetails());
    }


    private static Employee.EmployeeBuilder prepareEmployee(final UserRole role, final String email) {
        return Employee.builder()
                .firstName("employee")
                .lastName("Last name")
                .email(email)
                .role(role)
                .salary(2000.1)
                .password("password")
                .dateStarted(LocalDate.now().minusMonths(12L));
    }


    private Company getCompanyDetails() {
        return Company.builder()
                .companyName("name")
                .accountNumber("123243")
                .taxNumber("PL3566665")
                .address("Addddderreess")
                .build();
    }
}
