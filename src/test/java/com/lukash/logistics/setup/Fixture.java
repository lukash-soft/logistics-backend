package com.lukash.logistics.setup;

import com.auth0.jwt.JWT;
import com.lukash.logistics.configuration.security.constants.SecurityConstants;
import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.company.model.Company;
import com.lukash.logistics.feature.damageReport.model.DamageReport;
import com.lukash.logistics.feature.files.model.TransportOrderFile;
import com.lukash.logistics.feature.transport.dto.TransportOrderCsvDTO;
import com.lukash.logistics.feature.transport.model.Cargo;
import com.lukash.logistics.feature.transport.model.TransportOrder;
import com.lukash.logistics.feature.transport.model.enums.QuantityType;
import com.lukash.logistics.feature.transport.model.enums.TransportStatus;
import com.lukash.logistics.feature.user.carrier.model.Carrier;
import com.lukash.logistics.feature.user.client.model.Client;
import com.lukash.logistics.feature.user.client.model.UserStatus;
import com.lukash.logistics.feature.user.employee.dto.EmployeeRegistrationDto;
import com.lukash.logistics.feature.user.employee.model.Employee;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.lukash.logistics.configuration.security.constants.SecurityConstants.EXPIRATION_TIME;
import static com.lukash.logistics.configuration.security.constants.SecurityConstants.ROLE_KEY;
import static com.lukash.logistics.configuration.security.constants.SecurityConstants.SECRET;

public class Fixture {
    private static final Random RANDOMIZER = new Random();
    public static final String EMPLOYEE_EMAIL = "testEmail@gmail.com";
    public static final String FILES_DIR = Paths.get("src", "test", "resources", "files").toAbsolutePath().toString();


    public static DamageReport prepareDamageReport(TransportOrder transportOrder, List<TransportOrderFile> orderFiles) {
        return DamageReport.builder()
                .description("Test descr damage report")
                .supervisor(transportOrder.getSupervisor())
                .transportOrder(transportOrder)
                .orderFile(orderFiles)
                .build();
    }

    public static List<TransportOrderFile> prepareOrderFiles(Employee employee, final int numberOfFiles, TransportOrder transportOrder) {

        return IntStream.rangeClosed(1, numberOfFiles).mapToObj(object ->
                        TransportOrderFile.builder()
                                .name("Test name")
                                .transportOrder(transportOrder)
                                .path("random")
                                .addedBy(employee)
                                .description("Test descr")
                                .build())
                .collect(Collectors.toList());
    }

    public static TransportOrderFile prepareOrderFile(Employee employee, TransportOrder transportOrder) {
        return TransportOrderFile.builder()
                .addedBy(employee)
                .name("Test name")
                .transportOrder(transportOrder)
                .path(FILES_DIR + "/orderFile.pdf")
                .description("Test descr")
                .dateAdded(LocalDate.now())
                .build();
    }

    public static UserDetails prepareUser(Employee employee) {
        final List<GrantedAuthority> permissions = List.of(new SimpleGrantedAuthority(employee.getRole().value));

        return new User(
                employee.getEmail(),
                employee.getPassword(),
                true,
                true,
                true,
                true,
                permissions);
    }

    public static TransportOrder prepareOrder(Carrier carrier, Client client, Employee employee) {
        Random random = new Random();

        return TransportOrder.builder()
                .carrier(carrier)
                .addressFrom("Address From test")
                .addressTo("Address To test")
                .dateStartedPlanned(LocalDate.of(2023, 1, 3))
                .dateStartedReal(LocalDate.of(2023, 1, 3))
                .dateEndedPlanned(LocalDate.now().plusDays(30))
                .cargo(prepareCargo())
                .carrierFee(1500.0)
                .totalPrice(3000.0)
                .vehicleNumber(getRandomNumber(random, 7))
                .client(client)
                .supervisor(employee)
                .build();
    }

    public static List<TransportOrder> prepareCompleteOrdersInTime(Client client, Employee employee, Carrier carrier) {
        List<TransportOrder> orders = new ArrayList<>();
        LocalDate endDatePlanned = LocalDate.now().withDayOfMonth(6).minusMonths(1);

        for (int i = 0; i < 4; i++) {
            TransportOrder order = Fixture.prepareOrder(carrier, client, employee);
            order.setDateEndedPlanned(endDatePlanned);
            order.setDateEndedReal(order.getDateEndedPlanned());
            order.setStatus(TransportStatus.COMPLETE);
            orders.add(order);
        }
        return orders;
    }

    public static List<TransportOrder> prepareCancelledOrders(Client client, Employee employee, Carrier carrier) {
        List<TransportOrder> orders = new ArrayList<>();
        LocalDate endDateReal = LocalDate.now().withDayOfMonth(6).minusMonths(1);

        for (int i = 0; i < 4; i++) {
            TransportOrder order = Fixture.prepareOrder(carrier, client, employee);
            order.setStatus(TransportStatus.CANCELLED);
            order.setDateEndedReal(endDateReal);
            orders.add(order);
        }
        return orders;
    }

    public static List<TransportOrder> prepareCompleteOrdersDelayed(Client client, Employee employee, Carrier carrier) {
        List<TransportOrder> orders = new ArrayList<>();
        LocalDate endDatePlanned = LocalDate.now().minusMonths(1);

        for (int i = 0; i < 4; i++) {
            TransportOrder order = Fixture.prepareOrder(carrier, client, employee);
            order.setStatus(TransportStatus.COMPLETE);
            order.setDateEndedPlanned(endDatePlanned);
            order.setDateEndedReal(order.getDateEndedPlanned().plusDays(2));
            orders.add(order);
        }
        return orders;
    }

    public static List<TransportOrder> prepareOrdersInProgressDelayed(Client client, Employee employee, Carrier carrier) {
        List<TransportOrder> orders = new ArrayList<>();
        LocalDate today = LocalDate.now();

        for (int i = 0; i < 4; i++) {
            TransportOrder order = Fixture.prepareOrder(carrier, client, employee);
            order.setStatus(TransportStatus.IN_PROGRESS);
            order.setDateEndedPlanned(today.minusDays(15));
            orders.add(order);
        }
        return orders;
    }

    public static List<TransportOrder> prepareOrdersInProgress(Client client, Employee employee, Carrier carrier) {
        List<TransportOrder> orders = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            TransportOrder order = Fixture.prepareOrder(carrier, client, employee);
            orders.add(order);
            TransportOrder orderInProgress = Fixture.prepareOrder(carrier, client, employee);
            orderInProgress.setStatus(TransportStatus.IN_PROGRESS);
            orders.add(orderInProgress);
        }
        return orders;
    }

    public static Cargo prepareCargo() {
        return Cargo.builder()
                .description("Test Cargo")
                .cargoType("Meat")
                .quantity(1500.0)
                .quantityType(QuantityType.KILOGRAMS)
                .build();
    }

    public static Carrier prepareCarrier(String companyName) {
        Random random = new Random();
        String accountNumber = getRandomNumber(random, 12);
        String taxNumber = getRandomNumber(random, 10);

        return Carrier.builder()
                .companyName(companyName)
                .address("TestAddresss")
                .taxNumber(taxNumber)
                .accountNumber(accountNumber)
                .build();
    }

    public static List<Carrier> prepareCarriers(int quantity) {
        return IntStream.rangeClosed(1, quantity)
                .mapToObj(object -> prepareCarrier("Company test name"))
                .collect(Collectors.toList());
    }

    public static Company prepareCompany(String companyName) {
        Random random = new Random();
        String accountNumber = getRandomNumber(random, 12);
        String taxNumber = getRandomNumber(random, 10);

        return Company.builder()
                .companyName(companyName)
                .taxNumber(taxNumber)
                .accountNumber(accountNumber)
                .address("Test Address")
                .build();
    }

    public static Client prepareClient(String companyName, UserStatus userStatus) {
        Random random = new Random();
        String accountNumber = getRandomNumber(random, 12);
        String taxNumber = getRandomNumber(random, 10);

        return Client.builder()
                .companyName(companyName)
                .accountNumber(accountNumber)
                .address("Test Address")
                .taxNumber(taxNumber)
                .userStatus(userStatus)
                .build();
    }

    public static List<Client> prepareClients(int quantity, UserStatus userStatus) {
        return IntStream.rangeClosed(1, quantity)
                .mapToObj(shift -> prepareClient("TestClient " + shift, userStatus))
                .collect(Collectors.toList());
    }

    public static Employee prepareEmployee(final UserRole role) {
        return prepareEmployee(role, RANDOMIZER.nextInt());
    }

    private static Employee prepareEmployee(final UserRole role, final int shift) {
        return Employee.builder()
                .firstName("First name " + shift)
                .lastName("Last name" + shift)
                .email("testEmail" + shift + "@com")
                .role(role)
                .salary(2000.1)
                .password("pass")
                .dateStarted(LocalDate.now())
                .build();
    }

    public static Employee prepareEmployee(final UserRole role, final String email) {
        return Employee.builder()
                .firstName("First name ")
                .lastName("Last name")
                .email(email)
                .role(role)
                .salary(2000.1)
                .password("pass")
                .dateStarted(LocalDate.now())
                .build();
    }

    private static String getRandomNumber(Random random, int maxSize) {
        int limitLeft = 48;
        int limitRight = 57;
        return random
                .ints(limitLeft, limitRight)
                .limit(maxSize)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
    }

    public static String prepareJwt(final Employee employee) {
        return SecurityConstants.TOKEN_PREFIX + JWT.create()
                .withSubject(employee.getEmail())
                .withClaim(ROLE_KEY, employee.getRole().value)
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(SECRET.getBytes()));
    }


    public static EmployeeRegistrationDto getRegistrationDto() {
        return EmployeeRegistrationDto.builder()
                .firstName("Test first")
                .lastName("Test lastName")
                .email("testEmail.com")
                .password("pass")
                .dateStarted(LocalDate.now())
                .role(UserRole.EMPLOYEE)
                .salary(2000.2)
                .build();
    }

    public static TransportOrderCsvDTO prepareOrderCsvDto(Client left, Carrier right) {
        return TransportOrderCsvDTO.builder()
                .carrierId(right.getId())
                .clientId(left.getId())
                .vehicleNumber("111111")
                .totalPrice(123.0)
                .carrierFee(123.0)
                .dateEndedPlanned(LocalDate.now().plusDays(12))
                .dateStartedPlanned(LocalDate.of(2023, 1, 1))
                .addressTo("England")
                .addressFrom("Poland")
                .cargoDescription("Test desc")
                .cargoType("Cigarettes")
                .quantity(12.0)
                .quantityType(QuantityType.KILOGRAMS)
                .build();
    }

    public static MultipartFile createMultipartFile(final File file) {
        try (InputStream input = new FileInputStream(file)) {
            return new MockMultipartFile(file.getName(), file.getName(), null, input);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }
}
