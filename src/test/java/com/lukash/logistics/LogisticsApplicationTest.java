package com.lukash.logistics;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class LogisticsApplicationTest {

    @Test
    void shouldThrowException() {
        assertThatThrownBy(() -> LogisticsApplication.main(null)).isInstanceOf(IllegalArgumentException.class);
    }
}
