package com.lukash.logistics.configuration.security.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukash.logistics.configuration.security.constants.SecurityConstants;
import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.user.employee.dto.EmployeeDto;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ControllerSanityCheckTest extends IntegrationTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    MockMvc mockMvc;

    @AfterEach
    void cleanup() {
        employeeRepository.deleteAll();
    }

    @Test
    void shouldReceive401IfNotAuthenticated() throws Exception {
        //given
        employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        //when
        final MvcResult result = mockMvc
                .perform(get("/employees"))
                .andExpect(status().isForbidden())
                .andReturn();
        //then
        assertThat(result.getResponse().getContentAsString()).isBlank();
    }

    @Test
    void shouldReceive200IfAuthenticated() throws Exception {
        //given
        final Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.MANAGER));
        final String jwt = Fixture.prepareJwt(employee);

        //when
        final MvcResult result = mockMvc
                .perform(get("/employees").header(SecurityConstants.HEADER_STRING, jwt))
                .andExpect(status().isOk())
                .andReturn();

        //then
        assertThat(result.getResponse().getContentAsString()).isNotBlank();
    }

    @Test
    void shouldBeAbleToUpdateSalaryIfIsOwner() throws Exception {
        //given
        final Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        final Employee manager = employeeRepository.save(Fixture.prepareEmployee(UserRole.MANAGER));

        final String jwt = Fixture.prepareJwt(manager);
        //when
        final EmployeeDto dto = EmployeeDto.builder()
                .id(employee.getId())
                .salary(400)
                .build();

        final MvcResult result = mockMvc
                .perform(put("/employees/salary")
                        .contentType(APPLICATION_JSON_VALUE)
                        .header(SecurityConstants.HEADER_STRING, jwt)
                        .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andReturn();

        //then
        assertThat(result.getResponse().getContentAsString()).isEqualTo("");
        assertThat(employeeRepository.findById(employee.getId()).get())
                .extracting(Employee::getSalary)
                .isEqualTo(dto.getSalary());
    }

    @Test
    void shouldNoBeAbleToUpdateSalaryIfIsEmployee() throws Exception {
        //given
        final Employee employee = employeeRepository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));
        final Employee manager = employeeRepository.save(Fixture.prepareEmployee(UserRole.MANAGER));

        final String jwt = Fixture.prepareJwt(employee);
        //when
        final EmployeeDto dto = EmployeeDto.builder()
                .id(employee.getId())
                .salary(400)
                .build();

        final MvcResult result = mockMvc
                .perform(put("/employees/salary")
                        .contentType(APPLICATION_JSON_VALUE)
                        .header(SecurityConstants.HEADER_STRING, jwt)
                        .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isForbidden())
                .andReturn();
        //then
        assertThat(result.getResponse().getContentAsString()).isEqualTo("");
        assertThat(employeeRepository.findById(employee.getId()).get())
                .extracting(Employee::getSalary)
                .isEqualTo(employee.getSalary());
    }
}
