package com.lukash.logistics.configuration.security.service;

import com.lukash.logistics.configuration.security.constants.UserRole;
import com.lukash.logistics.feature.user.employee.model.Employee;
import com.lukash.logistics.feature.user.employee.repository.EmployeeRepository;
import com.lukash.logistics.setup.Fixture;
import com.lukash.logistics.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class UserDatabaseDetailsServiceTest extends IntegrationTest {

    @Autowired
    EmployeeRepository repository;
    @Autowired
    UserDatabaseDetailsService userDatabaseDetailsService;

    @AfterEach
    void cleanUp() {
        repository.deleteAll();
    }

    @Test
    void shouldBeActive() {
        //given
        final Employee employee = repository.save(Fixture.prepareEmployee(UserRole.EMPLOYEE));

        //when
        final UserDetails userDetails = userDatabaseDetailsService.loadUserByUsername(employee.getEmail());

        //then
        assertThat(userDetails.isAccountNonExpired()).isTrue();
    }


    @Test
    void shouldBeDisabled() {
        //given
        final Employee employee = Fixture.prepareEmployee(UserRole.EMPLOYEE);
        employee.setDateStarted(LocalDate.now().plusDays(12));
        employee.setDateEnded(LocalDate.now().minusDays(24));
        repository.save(employee);


        //when
        final UserDetails userDetails = userDatabaseDetailsService.loadUserByUsername(employee.getEmail());

        //then
        assertThat(userDetails.isAccountNonExpired()).isFalse();
    }
}
